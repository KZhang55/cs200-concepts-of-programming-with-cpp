// !! You don't need to edit this file !!

#ifndef _COURSE_MANAGER
#define _COURSE_MANAGER

#include "Course.h"

#include <vector>
using namespace std;

class CourseManager
{
    public:
    void DisplayCourseTable();
    void DisplayCourseTable(vector<Course> courses);
    void DisplayCourse( unsigned int index );
    void LoadCourses( string filename );
    void SaveCourses( string filename, vector<Course> courses );
    unsigned int GetCourseCount();

    vector<Course> SearchCoursesByDepartment(const vector<Course>& original, string searchTerm );
    vector<Course> SearchCoursesByTitle(const vector<Course>& original, string searchTerm );
    vector<Course> SearchCoursesByHours(const vector<Course>& original, string searchTerm );
    vector<Course> SearchCoursesByDescription(const vector<Course>& original, string searchTerm );

    vector<Course> GetCourses();
    vector<Course> BubbleSortCoursesByTitle(const vector<Course>& original);
    vector<Course> InsertionSortCoursesByHours(const vector<Course>& original);

    bool FindText( string searchMe, string findMe );

    void RunUnitTests();

    private:
    vector<Course> m_courses;
};

#endif
