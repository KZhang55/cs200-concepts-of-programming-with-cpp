/*
Build program:
g++ program2.cpp -o sum

Run: 
./sum START END
*/

#include <iostream> /* Use cout */
using namespace std;

int SumUp_Iter(int start, int end) {
  // 1. Create an integer variable named `sum`, initialize it to `0`.
  // 2. Create a FOR LOOP
  // ** Starting code: Create a counter `i`, initialize to `start`
  // ** Condition: Continue looping while `i` is less than or equal to `end`
  // ** Update: Add 1 to `i` each iteration
  // Within the for loop:
  //  2a. Add the value of `i` onto the value of `sum`. Make sure the `sum` variable is overwritten.
  // After the loop:
  // 3. Return `sum`.
}

int SumUp_Rec(int start, int end) {
  // 1. TERMINATING CASE: If the value of `start` is greater than `end`, then return `0`.
  // 2. RECURSIVE CASE: Return the value of `start` PLUS a call to `Sumup_Rec` with the arguments `start+1` and `end`.
}

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 3) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " start end" << endl;
    return 1; /* Exit with error code 1 */
  }

  int start = stoi(argumentList[1]);
  int end = stoi(argumentList[2]);
  int result;

  cout << endl << "ITERATIVE VERSION: ";
  result = SumUp_Iter(start, end);
  cout << result << endl;

  cout << endl << "RECURSIVE VERSION: ";
  result = SumUp_Rec(start, end);
  cout << result << endl;

  /* Quit program with code 0 (no errors) */
  cout << endl << "GOODBYE" << endl;
  return 0;
}
