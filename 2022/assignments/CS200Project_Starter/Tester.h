#ifndef _TESTER_H
#define _TESTER_H

/**
Unit tests to validate work
(No need to edit)
*/
class Tester
{
    public:
    void Test_Utilities();
    void Test_StoreItems();
    void Test_Books();
    void Test_Program();
};

#endif
