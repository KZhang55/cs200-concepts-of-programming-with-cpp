#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Functions.h"

int main()
{
  string contents = "We can dance if we want to\nWe can leave your friends behind\n'Cuz your friends don't dance and if they don't dance\nWell, they're no friends of mine.\n\nSay, we can go where we want to\nA place where they will never find\nAnd we can act like we come from out of this world\nLeave the real one far behind.";

  cout << left;

  bool done = false;
  while ( !done )
  {
    ClearScreen();
    // Display text
    cout << string( 80, '-' ) << endl;
    cout << contents << endl;
    cout << string( 80, '-' ) << endl;

    cout << setw( 20 ) << "INFO"        << setw( 20 ) << "MODIFY" << setw( 20 ) << "PROGRAM" << endl;
    cout << setw( 20 ) << "1. Size"     << setw( 20 ) << "6.  Combine" << setw( 20 ) << "11. Quit" << endl;
    cout << setw( 20 ) << "2. Letter"   << setw( 20 ) << "7.  Insert" << endl;
    cout << setw( 20 ) << "3. Find"     << setw( 20 ) << "8.  Remove" << endl;
    cout << setw( 20 ) << "4. Substr"   << setw( 20 ) << "9.  Replace #" << endl;
    cout << setw( 20 ) << "5. Order"    << setw( 20 ) << "10. Replace str" << endl;
    cout << string( 80, '-' ) << endl;
    int choice = GetChoice( 1, 12 );

    switch( choice )
    {
      case 1:  Menu_Size( contents );                             break;
      case 2:  Menu_Letter( contents );                           break;
      case 3:  Menu_Find( contents );                             break;
      case 4:  Menu_Substring( contents );                        break;
      case 5:  Menu_Order( contents );                            break;
      case 6:  Menu_Combine( contents );                          break;
      case 7:  Menu_Insert( contents );                           break;
      case 8:  Menu_Remove( contents );                           break;
      case 9:  Menu_ReplaceAt( contents );                        break;
      case 10: Menu_ReplaceString( contents );                    break;
      case 11: done = true; cout << "Goodbye." << endl;           break;
    }

    Pause();
  }

  return 0;
}
