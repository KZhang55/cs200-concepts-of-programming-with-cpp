/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include "UnitTests.h"

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Student.h"

void StudentTest()
{
    cout << string( 80, '-' ) << endl;
    cout << endl << "STUDENT TESTS" << endl;

    int testsPass = 0;
    int totalTests = 0;

    {
        Student student;
        student.Setup( "Bobbert" );
        if ( student.fullName == "Bobbert" )    { testsPass++; cout << "[PASS] "; }
        else                                    { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 1: Create a Student. Call student.Setup(\"Bobbert\"); Verify fullName was updated." << endl;
        cout << "\t Student student;        student.Setup(\"Bobbert\");" << endl;
        cout << "\t EXPECTATION:            student.fullName is \"Bobbert\"." << endl;
        cout << "\t ACTUAL:                 student.fullName is \"" << student.fullName << "\"." << endl;
        cout << endl;
    }

    {
        Student student;
        student.Setup( "Kochai" );
        if ( student.fullName == "Kochai" )    { testsPass++; cout << "[PASS] "; }
        else                                    { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 2: Create a Student. Call student.Setup(\"Kochai\"); Verify fullName was updated." << endl;
        cout << "\t Student student;        student.Setup(\"Kochai\");" << endl;
        cout << "\t EXPECTATION:            student.fullName is \"Kochai\"." << endl;
        cout << "\t ACTUAL:                 student.fullName is \"" << student.fullName << "\"." << endl;
        cout << endl;
    }

    {
        Student student;
        student.fullName = "Astha";
        if ( student.GetName() == "Astha" )    { testsPass++; cout << "[PASS] "; }
        else                                    { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 3: Create a Student. Set student.fullName. Check that student.GetName() returns the right value." << endl;
        cout << "\t Student student;        student.fullName = \"Astha\";" << endl;
        cout << "\t EXPECTATION:            student.GetName() is \"Astha\"." << endl;
        cout << "\t ACTUAL:                 student.GetName() is \"" << student.fullName << "\"." << endl;
        cout << endl;
    }

    {
        Student student;
        student.fullName = "Lyon";
        if ( student.GetName() == "Lyon" )    { testsPass++; cout << "[PASS] "; }
        else                                    { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 4: Create a Student. Set student.fullName. Check that student.GetName() returns the right value." << endl;
        cout << "\t Student student;        student.fullName = \"Lyon\";" << endl;
        cout << "\t EXPECTATION:            student.GetName() is \"Lyon\"." << endl;
        cout << "\t ACTUAL:                 student.GetName() is \"" << student.fullName << "\"." << endl;
        cout << endl;
    }

    {
        Student student;
        if ( student.totalGrades == 0 )    { testsPass++; cout << "[PASS] "; }
        else                               { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 5: Create a new Student. Make sure totalGrades defaults to 0." << endl;
        cout << "\t Student student;" << endl;
        cout << "\t EXPECTATION:    student.totalGrades is 0." << endl;
        cout << "\t ACTUAL:         student.totalGrades is " << student.totalGrades << "." << endl;
        cout << endl;
    }

    {
        Student student;
        student.AddGrade( 4.0 );
        student.AddGrade( 3.0 );
        if      ( student.totalGrades != 2 )                                    { cout << "[FAIL] totalGrades "; }
        else if ( student.grades[0] < 3.999 || student.grades[0] > 4.0001 )       { cout << "[FAIL] grades[0] "; }
        else if ( student.grades[1] < 2.999 || student.grades[1] > 3.0001 )       { cout << "[FAIL] grades[1] "; }
        else if ( student.GetGpa() < 3.499  || student.GetGpa() > 3.5001 )        { cout << "[FAIL] GetGpa()"; }
        else                                                                    { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << fixed << setprecision( 1 );
        cout << "TEST 6: Create a new Student. Add 2 grades. Make sure totalGrades matches, and grades are stored." << endl;
        cout << "\t Student student;    student.AddGrade( 4.0 );    student.AddGrade( 3.0 );" << endl;
        cout << "\t EXPECTATION:    student.totalGrades is 2." << endl;
        cout << "\t ACTUAL:         student.totalGrades is " << student.totalGrades << "." << endl;
        cout << "\t EXPECTATION:    student.grades[0] is 4.0." << endl;
        cout << "\t ACTUAL:         student.grades[0] is " << student.grades[0] << "." << endl;
        cout << "\t EXPECTATION:    student.grades[1] is 3.0." << endl;
        cout << "\t ACTUAL:         student.grades[1] is " << student.grades[1] << "." << endl;
        cout << "\t EXPECTATION:    student.GetGpa() is 3.5." << endl;
        cout << "\t ACTUAL:         student.GetGpa() is " << student.GetGpa() << "." << endl;
        cout << endl;
    }

    {
        Student student;
        student.AddGrade( 3.2 );
        student.AddGrade( 3.4 );
        student.AddGrade( 3.6 );
        if      ( student.totalGrades != 3 )                                    { cout << "[FAIL] totalGrades "; }
        else if ( student.grades[0] < 3.199 || student.grades[0] > 3.2001 )       { cout << "[FAIL] grades[0] "; }
        else if ( student.grades[1] < 3.399 || student.grades[1] > 3.4001)        { cout << "[FAIL] grades[1] "; }
        else if ( student.grades[2] < 3.599 || student.grades[2] > 3.6001 )       { cout << "[FAIL] grades[2] "; }
        else if ( student.GetGpa() < 3.399  || student.GetGpa() > 3.4001 )        { cout << "[FAIL] GetGpa()"; }
        else                                                                    { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << fixed << setprecision( 1 );
        cout << "TEST 6: Create a new Student. Add 3 grades. Make sure totalGrades matches, and grades are stored." << endl;
        cout << "\t Student student;    student.AddGrade( 3.2 );    student.AddGrade( 3.4 );    student.AddGrade( 3.2 );" << endl;
        cout << "\t EXPECTATION:    student.totalGrades is 3." << endl;
        cout << "\t ACTUAL:         student.totalGrades is " << student.totalGrades << "." << endl;
        cout << "\t EXPECTATION:    student.grades[0] is 3.2." << endl;
        cout << "\t ACTUAL:         student.grades[0] is " << student.grades[0] << "." << endl;
        cout << "\t EXPECTATION:    student.grades[1] is 3.4." << endl;
        cout << "\t ACTUAL:         student.grades[1] is " << student.grades[1] << "." << endl;
        cout << "\t EXPECTATION:    student.grades[1] is 3.6." << endl;
        cout << "\t ACTUAL:         student.grades[1] is " << student.grades[2] << "." << endl;
        cout << "\t EXPECTATION:    student.GetGpa() is 3.4." << endl;
        cout << "\t ACTUAL:         student.GetGpa() is " << student.GetGpa() << "." << endl;
        cout << endl;
    }

    cout << endl << string( 80, '*' ) << endl
        << testsPass << " tests passed out of " << totalTests << " total tests"
        << endl << string( 80, '*' ) << endl;
}
