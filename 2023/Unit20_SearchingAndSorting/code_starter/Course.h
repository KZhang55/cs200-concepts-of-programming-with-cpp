// !! You don't need to edit this file !!

#ifndef _COURSE_H
#define _COURSE_H

#include <string>
using namespace std;

class Course {
public:
  Course();
  Course(string code, string title, string hours, string description);
  void Setup(string code, string title, string hours, string description);
  void TableDisplay();
  void FullDisplay();

  string GetCode() const;
  string GetTitle() const;
  string GetHours() const;
  string GetDescription() const;

  void SetCode(string code);
  void SetTitle(string title);
  void SetHours(string hours);
  void SetDescription(string description);

  // Equivalence operator
  friend bool operator==(const Course &left, const Course &right);
  friend bool operator!=(const Course &left, const Course &right);

private:
  string m_code;
  string m_title;
  string m_hours;
  string m_description;
};

#endif
