// !! You don't need to edit this file !!

#include "Course.h"
#include <iomanip>
#include <iostream>
using namespace std;

Course::Course() {
  m_code = "unset";
  m_title = "unset";
  m_hours = "unset";
  m_description = "unset";
}

Course::Course(string code, string title, string hours, string description) {
  Setup(code, title, hours, description);
}

void Course::Setup(string code, string title, string hours,
                   string description) {
  SetCode(code);
  SetTitle(title);
  SetHours(hours);
  SetDescription(description);
}

bool operator==(const Course &left, const Course &right) {
  return (left.m_code == right.m_code &&
          left.m_description == right.m_description &&
          left.m_hours == right.m_hours && left.m_title == right.m_title);
}

bool operator!=(const Course &left, const Course &right) {
  return !(left == right);
}

void Course::TableDisplay() {
  string shortTitle = m_title;
  if (shortTitle.size() > 50) {
    shortTitle = m_title.substr(0, 46) + "...";
  }

  string shortDesc = m_description;
  if (shortDesc.size() > 30) {
    shortDesc = shortDesc.substr(0, 24) + "...";
  }

  cout << left << setw(10) << m_code << setw(10) << m_hours << setw(50)
       << shortTitle << setw(30) << shortDesc << endl;
}

void Course::FullDisplay() {
  cout << left << "DEPT:  " << m_code << endl
       << "TITLE: " << m_title << endl
       << "HOURS: " << m_hours << endl
       << "DESCRIPTION:" << endl
       << m_description << endl;
}

string Course::GetCode() const { return m_code; }

string Course::GetTitle() const { return m_title; }

string Course::GetHours() const { return m_hours; }

string Course::GetDescription() const { return m_description; }

void Course::SetCode(string code) { m_code = code; }

void Course::SetTitle(string title) { m_title = title; }

void Course::SetHours(string hours) { m_hours = hours; }

void Course::SetDescription(string description) { m_description = description; }
