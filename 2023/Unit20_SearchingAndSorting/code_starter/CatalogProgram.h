// !! You don't need to edit this file !!

#ifndef _CATALOG_PROGRAM
#define _CATALOG_PROGRAM

#include "CourseManager.h"

#include <string>
using namespace std;

class CatalogProgram {
public:
  void Run();

  void Menu_Main();
  void Menu_RunUnitTests();
  void Menu_ViewCourseList();
  void Menu_ViewCourseByIndex();
  void Menu_SearchCoursesByDepartment();
  void Menu_SearchCoursesByTitle();
  void Menu_SearchCoursesByHours();
  void Menu_SearchCoursesByDescription();
  void Menu_SaveListByDepartment();
  void Menu_SaveListByTitle();
  void Menu_SaveListByHours();

private:
  CourseManager m_courseManager;

  void Clear();
  void Header(string text);
  int GetValidInput(int min, int max);
  void DisplayNumberMenu(vector<string> options);
  void EnterToContinue(bool ignore = true);
};

#endif
