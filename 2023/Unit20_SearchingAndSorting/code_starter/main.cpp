// !! You don't need to edit this file !!

#include "CatalogProgram.h"

int main()
{
    CatalogProgram program;
    program.Run();

    return 0;
}
