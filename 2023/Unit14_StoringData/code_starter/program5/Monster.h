#ifndef _MONSTER
#define _MONSTER

#include <string>
using namespace std;

struct Monster
{
  string name;
  int level;
  float gold;
};

#endif
