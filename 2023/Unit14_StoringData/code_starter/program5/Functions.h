#ifndef _FUNCTIONS
#define _FUNCTIONS

#include "Monster.h"

#include <vector>
using namespace std;

void Menu_CreateMonster( vector<Monster>& monsterList );
void Menu_ViewAllMonsters( const vector<Monster>& monsterList );
void Menu_ViewOneMonster( const  vector<Monster>& monsterList );

int GetChoice( int min, int max );


#endif
