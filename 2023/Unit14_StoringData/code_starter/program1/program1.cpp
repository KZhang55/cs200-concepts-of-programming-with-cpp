#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
  // OPEN THE DATA FILE
  // 1. Create a string to store a `filename`. Store "websites.txt".
  // 2. Create an input file stream that opens up the filename.
  //      * If opening the input file fails, display an error and exit with a return code.

  
  // CREATE THE DATA STRUCTURE
  // 3. Create a const integer named `WEBSITE_COUNT`, assign it a value of 20.
  // 4. Create a traditional C++ array named `infoList` with the size `WEBSITE_COUNT`.

  
  // LOAD FROM THE FILE AND INTO THE DATA STRUCTURE
  // 5. Create a string variable named `buffer`. This is where data will be loaded to before it's stored.
  // 6. Create an unsigned integer named `index` and initialize it to 0.
  // 7. Create a while loop that continues looping while `input` is able to read into the `buffer`. Within the loop:
  //    * Assign the element in `infoList` at the `index` position to the `buffer`'s value.
  //    * Add 1 to `index`.
  //    * If `index` is greater than or equal to the `WEBSITE_COUNT`, use break to stop reading.
  
  
  // DISPLAY LOADED DATA FROM THE DATA STRUCTURE
  // 8. Reinitialize `index` to 0.
  // 9. Create a while loop. While `index` is less than the `WEBSITE_COUNT`, do the following:
  //    * Display the current `index` and display the element at the index position, `infoList[index]`.
  //    * Add 1 to `index` 
  
  
  return 0;
}
