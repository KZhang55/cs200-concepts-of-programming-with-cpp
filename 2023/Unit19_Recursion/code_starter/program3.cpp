/*
Build program:
g++ program2.cpp -o display

Run:
./display
*/

#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

void Display_Iter(vector<string> list) {
  // 1. Create a for loop:
  // ** STARTER: Create an integer `i` and initialize it to `0`.
  // ** CONDITION: Continue looping while `i` is less than the `list.size()`.
  // ** UPDATE: Add 1 to `i` each iteration.
  // Within the loop:
  //  1a. Display the index and value of the item at that index:
  //      `cout << setw( 3 ) << i << setw( 10 ) << list[i];`
}

void Display_Rec(vector<string> list, unsigned int i) {
  // 1. TERMINATING CASE: If `i` is greater than or equal to `list.size()` then return.
  // 2. ACTION: Display the index and value of the item at that index:
  //            `cout << setw( 3 ) << i << setw( 10 ) << list[i];`
  // 3. RECURSIVE CALL: Call `Display_Rec`, passing in the `list` and `i+1`.
}

int main() {
  vector<string> list1 = {"CS134", "CS200", "CS235"};
  vector<string> list2 = {"Kabe", "Luna", "Pixel", "Korra"};
  
  cout << left;

  cout << endl << "ITERATIVE VERSIONS:" << endl;
  cout << endl << "List 1: " << endl;
  Display_Iter(list1);
  cout << endl << endl << "List 2: " << endl;
  Display_Iter(list2);

  cout << endl << endl << "RECURSIVE VERSIONS:" << endl;
  cout << endl << "List 1: " << endl;
  Display_Rec(list1, 0);
  cout << endl << endl << "List 2: " << endl;
  Display_Rec(list2, 0);

  /* Quit program with code 0 (no errors) */
  cout << endl << endl << "GOODBYE" << endl;
  return 0;
}
