#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <string>
using namespace std;

int GetChoice( int min, int max );
void ClearScreen();
void Pause();

void Menu_Size( const string& str );
void Menu_Letter( const string& str );
void Menu_Find( const string& str );
void Menu_Substring( const string& str );
void Menu_Order( const string& str );
void Menu_Combine( string& str );
void Menu_Insert( string& str );
void Menu_Remove( string& str );
void Menu_ReplaceAt( string& str );
void Menu_ReplaceString( string& str );

#endif
