#include "Functions.h"
#include <iostream>
#include <string>
using namespace std;

int GetChoice( int min, int max )
{
  cout << "CHOICE: ";
  int choice;
  cin >> choice;

  while ( choice < min || choice > max )
  {
    cout << "INVALID, TRY AGAIN: ";
    cin >> choice;
  }

  return choice;
}

void ClearScreen()
{
  system( "clear" );
}

void Pause()
{
  string a;
  cout << endl << "PRESS ENTER TO CONTINUE" << endl;
  getline( cin, a );
}

void Menu_Size( const string& str )
{
  cout << "The size of the string is: ";
  // TODO: cout the size
  cout << 0 << endl;

  cin.ignore();
}

void Menu_Letter( const string& str )
{
  int index;
  cout << "Enter an index: ";
  cin >> index;
  cin.ignore();

  cout << "Character #" << index << " is: \"";
  // TODO: cout the character of `str` at this index.
  cout << 0;
  cout << "\"" << endl;

  cin.ignore();
}

void Menu_Find( const string& str )
{
  string findme;
  cout << "Enter a string to find: ";
  cin.ignore();
  getline( cin, findme );

  int foundPosition = -1;
  // TODO: Set `foundPosition` to the result of the string find function.

  if ( foundPosition == string::npos )
  {
    cout << "String not found" << endl;
  }
  else
  {
    cout << "String found at position " << foundPosition << endl;
  }
}

void Menu_Substring( const string& str )
{
  int position;
  cout << "Enter the starting position: ";
  cin >> position;

  int length;
  cout << "Enter the amount of characters to grab: ";
  cin >> length;

  string substring = "NOT SET";
  // TODO: Use the substr function to get a subset of characters from `str`
  cout << "The substring is \"" << substring << "\"" << endl;

  cin.ignore();
}

void Menu_Order( const string& str )
{
  int positionA;
  cout << "Enter first position: ";
  cin >> positionA;
  string substrA = str.substr( positionA, 1 );

  int positionB;
  cout << "Enter second position: ";
  cin >> positionB;
  string substrB = str.substr( positionB, 1 );

  cout << endl << "Comparing \"" << substrA << "\" with \"" << substrB << "\"" << endl;

  int result = 0;
  // TODO: Use the compare function to get a result
  cout << "Compare result = " << result << endl;

  if ( result < 0 )
  {
    cout << "\"" << substrA << "\" is less than \"" << substrB << "\"" << endl;
  }
  else if ( result > 0 )
  {
    cout << "\"" << substrA << "\" is greater than \"" << substrB << "\"" << endl;
  }
  else
  {
    cout << "\"" << substrA << "\" is the same as \"" << substrB << "\"" << endl;
  }

  cin.ignore();
}

void Menu_Combine( string& str )
{
  string newString;
  cout << "Enter a string to add to the end: ";
  cin.ignore();
  getline( cin, newString );

  // TODO: Use the + operator to add the newString onto str

  cout << "String has been updated to \n\"" << str << "\"" << endl;
}

void Menu_Insert( string& str )
{
  int position;
  cout << "Enter a position to insert at: ";
  cin >> position;

  string newString;
  cout << "Enter a string to insert: ";
  cin.ignore();
  getline( cin, newString );

  // TODO: Use the insert function to insert newString into str

  cout << "String has been updated to \n\"" << str << "\"" << endl;
}

void Menu_Remove( string& str )
{
  int position;
  cout << "Enter a position to remove from: ";
  cin >> position;

  int length;
  cout << "Enter the amount of characters to remove: ";
  cin >> length;

  // TODO: Use the erase function to remove part of str

  cout << "String has been updated to \n\"" << str << "\"" << endl;

  cin.ignore();
}

void Menu_ReplaceAt( string& str )
{
  int position;
  cout << "Enter a position to remove from: ";
  cin >> position;

  int length;
  cout << "Enter the amount of characters to remove: ";
  cin >> length;

  string newString;
  cout << "Enter a string to insert: ";
  cin.ignore();
  getline( cin, newString );

  // TODO: Use the replace function to replace a portion of str

  cout << "String has been updated to \n\"" << str << "\"" << endl;
}

void Menu_ReplaceString( string& str )
{
  string oldString;
  cout << "Enter a string to look for: ";
  cin.ignore();
  getline( cin, oldString );

  string newString;
  cout << "Enter a string to replace it with: ";
  getline( cin, newString );

  // TODO: Use the find function to get the position of the string to replace

  // TODO: Use the size function to get the size of how much space you're replacing

  // TODO: Use the replace function to replace the old string with the new string

  cout << "String has been updated to \n\"" << str << "\"" << endl;
}

