#ifndef _BOOK
#define _BOOK

#include <string>
using namespace std;

struct Book
{
  string title;
  string author;
  string isbn;
  float price;
  int rating;
  int quantity;
};

#endif
