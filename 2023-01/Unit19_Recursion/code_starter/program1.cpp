/*
Build program:
g++ program1.cpp -o countup

Run: 
./countup START END
*/

#include <iostream> /* Use cout */
using namespace std;

void CountUp_Iter(int start, int end) {
  // 1. Create a FOR LOOP
  // ** Starting code: Create a counter `i`, initialize to `start`
  // ** Condition: Continue looping while `i` is less than or equal to `end`
  // ** Update: Add 1 to `i` each iteration
  // Within the for loop:
  //  1a. Display the value of `i` and a space
  // The end
}

void CountUp_Rec(int start, int end) {
  // 1. TERMINATING CASE: If the `start` value is greater than the `end` value, then return.
  // 2. ACTION: Display the value of `start` and a space
  // 3. RECURSION: Call `CountUp_Rec` again, passing in `start+1` to move the start forward, and `end` staying the same.
}

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 3) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " start end" << endl;
    return 1; /* Exit with error code 1 */
  }

  int start = stoi(argumentList[1]);
  int end = stoi(argumentList[2]);

  cout << endl << endl << "ITERATIVE VERSION:" << endl;
  CountUp_Iter(start, end);

  cout << endl << endl << "RECURSIVE VERSION:" << endl;
  CountUp_Rec(start, end);

  /* Quit program with code 0 (no errors) */
  cout << endl << endl << "GOODBYE" << endl;
  return 0;
}
