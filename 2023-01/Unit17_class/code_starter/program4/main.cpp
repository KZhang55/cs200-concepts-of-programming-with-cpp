#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
using namespace std;

#include "Recipe.h"
#include "UnitTests.h"

int main() {
  bool done = false;
  srand(time(NULL));

  RecipeTest();
  
  // MAKE SURE TESTS ALL PASS BEFORE STARTING ON THE PROGRAM.
  

  cout << endl << "RECIPE PROGRAM" << endl;

  // 1. Create a Recipe object variable

  // 2. Create a `string input;` variable.

  // 3. Ask the user to enter the recipe name. Use `getline(cin, input);` to read the name.

  // 4. Use `recipe.SetName(input);` to set the recipe's name.

  // 5. Repeat steps 2 - 4 for getting the recipe Instructions.

  // 6. Ask the user how many total ingredients. Create an `int totalIngredients` variable, 
  // and cin the user's selection into this variable.

  // 7. Create temporary variables `newName`, `newUnit` (both strings)
  // and `newAmount` (a float).

  // 8. Create a for loop that uses an integer variable named `counter`.
  // The for loop should start counter at 1, and loop while counter is less than or equal to totalIngredients.
  // Within the loop:
  
  //   8a. Ask the user to enter the name of the ingredient. 
  //    Use getline to store the user's input in the `newName` variable.
  
  //   8b. Ask the user to enter the unit of measurement of the ingredient.
  //    Use getline to store the user's input in the `newUnit` variable.

  //   8c. Ask t he user to enter the amount of that ingredient.
  //    Use cin >> to store the user's input in the `newAmount` variable.

  //   8d. Call `recipe.AddIngredient( newName, newAmount, newUnit );`
  //     To add the new ingredient

  // AFTER THE FOR LOOP:
  // 9. Once the for loop is over, use `recipe.Display();` to display the finalized recipe.
  
  return 0;
}
