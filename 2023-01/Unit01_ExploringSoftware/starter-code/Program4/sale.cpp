#include <iomanip>
#include <iostream>
using namespace std;
int main(int argumentCount, char *argumentList[]) {
  if (argumentCount < 3) {
    cout << "NOT ENOUGH ARGUMENTS! Expected form:" << endl;
    cout << argumentList[0] << " price percentOff" << endl;
    return 1;
  }

  float price = stof(argumentList[1]);
  float salePercent = stof(argumentList[2]);

  float dollarsOff = price * salePercent / 100;
  float reducedPrice = price - dollarsOff;

  cout << setprecision(2) << fixed;
  cout << "Original price: $" << price << endl;
  cout << "Sale amount:    %" << salePercent << endl;
  cout << "Amount off:    -$" << dollarsOff << endl;
  cout << "\033[0;33m"; // set color
  cout << "Final price:    $" << reducedPrice << endl;

  return 0;
}