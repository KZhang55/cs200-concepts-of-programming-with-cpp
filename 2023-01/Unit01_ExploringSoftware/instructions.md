# Instructions  

For this assignment we will be exploring some **command line** programs, including some basic programs written in C++. Throughout this semester we will be writing command line programs as we learn the basics of C++.

---

## Program 1: Zork

First, we will play Zork, a text adventure from 1981. Go to https://playclassic.games/games/adventure-dos-games-online/play-zork-great-underground-empire-online/play/ and hit the play button to begin the game.

To play, you can use commands line **NORTH, WEST, EAST, SOUTH** to move around, or commands like **OPEN MAILBOX**, **READ LEAFLET** to interact with the environment. (You can see a list of commands here: https://zork.fandom.com/wiki/Command_List)

Take five or so minutes to play the game, explore around the environment and such.

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 1. What were your impressions of the interface? Was it easy to use? Hard to use? What would you have changed about it to make it more user friendly?

---

## Program 2: Command line programs with arguments

Next, we're going to look at some **command line programs** we can use here in Replit. Make sure to open the **Shell** or **Console** view. You can drag it around so that you can read the instructions while also working in the Shell.

By default, the shell will have some text already:

```
~/U01EX-Exploring-Software$ 
```

We can enter commands after the `$` sign.

⬜

### ls - LIST items in the directory

For example, the `ls` (LS, but lower case) will **L**i**S**t the files and folders in the current directory:

```
~/U01EX-Exploring-Software$ ls
Program3  Program4  Program5  replit.nix
```

We can also pass an **argument**, which are additional options, after the name of the program. With the `ls` program, if we put `-l` (dash L), it will list the directory contents another way:

```
~/U01EX-Exploring-Software$ ls -l
total 4
drwxr-xr-x 1 runner runner 92 Sep 18 22:18 Program3
drwxr-xr-x 1 runner runner 32 Dec 26 20:22 Program4
drwxr-xr-x 1 runner runner 22 Dec 26 20:22 Program5
-rw-r--r-- 1 runner runner 81 Jan 18  2022 replit.nix
```

The `-l` option means to use a "long listing format".

We can also use the `-R` option to not only display the contents of the *current* directory, but **recurse** into each sub-folder and display all contents:

```
~/U01EX-Exploring-Software$ ls -R
.:
Program3  Program4  Program5  replit.nix

./Program3:
hello.txt     program.cpp
moretext.txt  simplefile.txt

./Program4:
sale.cpp  temp.cpp

./Program5:
zombies.cpp
```

And the arguments can be combined further:

```
~/U01EX-Exploring-Software$ ls -lR
.:
total 4
drwxr-xr-x 1 runner runner 92 Sep 18 22:18 Program3
drwxr-xr-x 1 runner runner 32 Dec 26 20:22 Program4
drwxr-xr-x 1 runner runner 22 Dec 26 20:22 Program5
-rw-r--r-- 1 runner runner 81 Jan 18  2022 replit.nix

./Program3:
total 16
-rw-r--r-- 1 runner runner 13 Dec 26 20:22 hello.txt
-rw-r--r-- 1 runner runner 16 Dec 26 20:22 moretext.txt
-rw-r--r-- 1 runner runner 91 Dec 26 20:22 program.cpp
-rw-r--r-- 1 runner runner 16 Dec 26 20:22 simplefile.txt

./Program4:
total 8
-rw-r--r-- 1 runner runner 752 Dec 26 20:22 sale.cpp
-rw-r--r-- 1 runner runner 708 Dec 26 20:22 temp.cpp

./Program5:
total 8
-rw-r--r-- 1 runner runner 6925 Dec 26 20:22 zombies.cpp
```

⬜

### pwd - Present Working Directory

The `pwd` command shows us what path we are currently in.

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 2. When you enter `pwd` in the Shell, copy/paste the resulting text that is shown.

⬜

### cd - Change Directory

The `cd` command allows us to change which directory (folder) we're currently inside of.

Enter in `cd` without any arguments, it will move us to the **root directory**:

```
~/U01EX-Exploring-Software$ cd
~$ pwd
/home/runner
```

If we enter `ls`, we can see our project folder:

```
~$ ls
U01EX-Exploring-Software
```

And we can use `cd U01EX-Exploring-Software` to go back into the project directory:

```
~$ cd U01EX-Exploring-Software/
~/U01EX-Exploring-Software$ ls
Program3  Program4  Program5  replit.nix
```

**⚠️ Note: you can partially type the name of a file or folder, then hit the TAB key to auto-complete it - this way, you don't have to type the entire name in! ⚠️**


**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 3. From the `~/U01EX-Exploring-Software$` path, use `cd Program3` and then `ls`. Write down the results in your notes.

You can also use `cd ..` to go **back one folder**. This is useful if you're inside a folder but want to go back:

```
~/U01EX-Exploring-Software/Program3$ cd ..
~/U01EX-Exploring-Software$ 
```

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 4. From the `~/U01EX-Exploring-Software/Program3$` path, use `cd ..` to go backwards. Then use the cd command to go into Program4 and display its contents. Write down Program4's ls results in your notes.

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 5. Navigate into the Program5 folder using the commands we've learned. Use the ls command to get the list of items in the Program5 folder and add it to your notes.

---

## Program 3: A super simple C++ program

From the **Files** view in Replit (the very left-hand bar), open the Program3 folder and open **program.cpp**. .cpp files are **C++ source code files**. Within program.cpp, you'll see a basic program:

```cpp
#include <iostream>
int main() {
  std::cout << "Hello, world!" << std::endl;
  return 0;
}
```

We will learn more about C++ code later, but here's what each line does:

1. `#include <iostream>` says to import the iostream (input output stream) library into the program. This is like an **import** in Python, and will allow us to use commands to output text to the screen.
2. `int main() {` is the starting point of all C++ programs. `main()` is a **function**, and we must include our code within. (We can eventually write our own functions later on.)
3. `std::cout << "Hello, world!" << std::endl;` is telling the program to use the `cout` (console-output) command from teh `std` (standard) library to display "Hello, world!" to the screen. The `endl` (end-line) will also add a new line to the end of the output.
4. `return 0;` is where the program ends. We return `0` because there were no errors that occurred.
5. Every function in C++ must be written within curly braces: `{ }` The last line of code is the closing curly brace for `main()`.

### Building and running the program

Navigate into the **Program3** folder from the Shell using the `cd` command. When you enter `ls`, it should show `program.cpp`:

```
~/U01EX-Exploring-Software/Program3$ ls
program.cpp
```

In order to **run** the program, we must first **build** (aka **compile**) it. The compilation process converts **source code** into **an executable file** - a file that can be run.

To do this, we are going to use the `g++` compiler program. We can run `g++` from the command line.

The first argument should be the source code file we want to build, and we can add additional options as needed. A basic build will look like this:

```
~/U01EX-Exploring-Software/Program3$ g++ program.cpp 
~/U01EX-Exploring-Software/Program3$ 
```

If there are **no build errors**, the g++ program won't say anything and just return you back to the Shell. Now when we type `ls`, we will see a new file:

```
~/U01EX-Exploring-Software/Program3$ ls
a.out  program.cpp
```

By default, when we build a program, g++ will name the executable file **a.out**.

We can then run the program by using `./` before the program name:

```
~/U01EX-Exploring-Software/Program3$ ./a.out 
Hello, world!
~/U01EX-Exploring-Software/Program3$ 
```

The program runs and quits and returns us to the Shell.

If we want to customize the name of the output executable file, we use the `-o` command, followed by a program name:

```
~/U01EX-Exploring-Software/Program3$ g++ program.cpp -o HelloProgram.exe
~/U01EX-Exploring-Software/Program3$ ./HelloProgram.exe 
Hello, world!
```

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 6. Copy the Shell commands where you built and ran this program and paste the commands into your notes, similar to the example run above.

---

## Program 4: C++ command line programs examples

Next, navigate into the Program4 folder. This folder contains two different C++ source files:

```
~/U01EX-Exploring-Software/Program4$ ls
sale.cpp  temp.cpp
```

Each one does something different. 

### temp.cpp - Temperature converter

Build the temp.cpp file like this: 

```
~/U01EX-Exploring-Software/Program4$ g++ temp.cpp -o temp.exe
```

When you try to run the program like before, we will get an error:

```
~/U01EX-Exploring-Software/Program4$ ./temp.exe NOT ENOUGH ARGUMENTS! Expected form:
./temp.exe temp C/F
```

This program wants **arguments** - it wants you to specify a temperature, and either C for Celsius or F for Farenheit.

We can re-run the program, specifying these two items:

```
~/U01EX-Exploring-Software/Program4$ ./temp.exe 32 F
32 F = 0 C
```

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 7. Run the temp.exe program to convert 100 F to Celsius and paste the result in your notes.
> 
> 8. Run the temp.exe program to convert 10 C to Farenheit and paste the result in your notes.

If we look in the temp.cpp source code, you can see the basic program, but you don't have to worry about understand it quite yet.

⬜

### sale.cpp - Sale price calculator

Build the sale program:

```
~/U01EX-Exploring-Software/Program4$ g++ sale.cpp -o sale.exe
```

When you try to run it, it will also tell you what arguments are required:

```
~/U01EX-Exploring-Software/Program4$ ./sale.exe NOT ENOUGH ARGUMENTS! Expected form:
./sale.exe price percentOff
```

A price is needed, and a percent off, so we can see how much a $9.95 item that is 45% off would cost:

```
~/U01EX-Exploring-Software/Program4$ ./sale.exe 9.95 45
Original price: $9.95
Sale amount:    %45.00
Amount off:    -$4.48
Final price:    $5.47
```

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 9. Run the sale.exe program to find out how much a $75.50 item would cost after a 35% off sale. Paste the results into your notes.


---

## Program 5: C++ text game program example

Finally, navigate into the Program5 folder. Build the `zombies.cpp` code into a program named `zombies.exe`. This program does not need any arguments, so go ahead and run it without: `./zombies.exe`

Play the game for a few minutes, then respond to the questions:

**✏️ ANSWER THE FOLLOWING IN YOUR NOTES:**
> 10. What were your impressions of the interface? Was it easy to use? Hard to use? What would you have changed about it to make it more user friendly?
>
> 11. Did the program allow invalid commands to be entered? What happened when invalid input was detected? Do you think this should be changed?

**⚠️ Note: If you want to stop the execution of a program early, use CTRL+C in the Shell to ESCAPE the program! ⚠️**

---

To turn in this assignment, copy your notes and paste them into the textbox on Canvas. See Canvas for more instructions.
