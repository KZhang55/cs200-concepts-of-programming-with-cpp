#include "CourseManager.h"
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

// - YOU WILL BE IMPLEMENTING THESE FUNCTIONS... -
// ---------------------------------------------------------

vector<Course> CourseManager::SearchCoursesByDepartment(const vector<Course>& original, string searchTerm) {
	vector<Course> matches;

	// TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY CODE

	return matches;
}

vector<Course> CourseManager::SearchCoursesByTitle(const vector<Course>& original, string searchTerm) {
	vector<Course> matches;

	// TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY TITLE

	return matches;
}

vector<Course> CourseManager::SearchCoursesByHours(const vector<Course>& original, string searchTerm) {
	vector<Course> matches;

	// TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY HOURS

	return matches;
}

vector<Course> CourseManager::SearchCoursesByDescription(const vector<Course>& original, string searchTerm) {
	vector<Course> matches;

	// TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY DESCRIPTION

	return matches;
}

// Bubble sort
vector<Course> CourseManager::BubbleSortCoursesByTitle(const vector<Course>& original) {
	vector<Course> sortCourses = original;

	// TODO: IMPLEMENT BUBBLE SORT HERE, SORT BY COURSE TITLES

	return sortCourses;
}

// Insertion sort
vector<Course> CourseManager::InsertionSortCoursesByHours(const vector<Course>& original) {
	vector<Course> sortCourses = original;

	// TODO: IMPLEMENT INSERTION SORT HERE, SORT BY COURSE HOURS

	return sortCourses;
}

// - YOU DON'T NEED TO IMPLEMENT ANYTHING BELOW THIS LINE! -
// ---------------------------------------------------------

void CourseManager::DisplayCourseTable() { DisplayCourseTable(m_courses); }

void CourseManager::DisplayCourseTable(vector<Course> courses) {
	cout << left << setw(7) << "ID" << setw(10) << "CODE" << setw(10) << "HOURS"
		<< setw(50) << "TITLE" << setw(30) << "DESCRIPTION" << endl
		<< string(107, '-') << endl;
	for (unsigned int i = 0; i < courses.size(); i++) {
		cout << left << setw(7) << string(to_string(i) + ". ");
		courses[i].TableDisplay();
	}
}

void CourseManager::DisplayCourse(unsigned int index) {
	if (index < 0 || index >= m_courses.size()) {
		cout << "ERROR: Invalid index!" << endl;
		return;
	}

	m_courses[index].FullDisplay();
}

void CourseManager::LoadCourses(string filename) {
	ifstream input(filename);
	Course newCourse;
	string buffer;
	int counter = 0;
	while (getline(input, buffer)) {
		if (counter % 5 == 0) {
			newCourse.SetCode(buffer);
		}
		else if (counter % 5 == 1) {
			newCourse.SetTitle(buffer);
		}
		else if (counter % 5 == 2) {
			newCourse.SetHours(buffer);
		}
		else if (counter % 5 == 3) {
			newCourse.SetDescription(buffer);
		}
		else if (counter % 5 == 4) {
			m_courses.push_back(newCourse);
		}
		counter++;
	}
}

void CourseManager::SaveCourses(string filename, vector<Course> courses) {
	ofstream output(filename);
	output << left;
	for (unsigned int i = 0; i < courses.size(); i++) {
		output << setw(10) << i << setw(10) << courses[i].GetCode() << setw(10)
			<< courses[i].GetHours() << setw(50) << courses[i].GetTitle()
			<< endl;
	}
}

bool CourseManager::FindText(string searchMe, string findMe) {
	string lowerCaseSearch = "";
	string lowerCaseFind = "";

	for (unsigned int i = 0; i < searchMe.size(); i++) {
		lowerCaseSearch += tolower(searchMe[i]);
	}

	for (unsigned int i = 0; i < findMe.size(); i++) {
		lowerCaseFind += tolower(findMe[i]);
	}

	return (lowerCaseSearch.find(lowerCaseFind) != string::npos);
}

void CourseManager::RunUnitTests() {
	{ // Search by department test
		vector<Course> courseList = {
			Course("AAA", "Intro to Potato Peeling", "3",
				   "How to peel potatoes and avoid peeling yourself"),
			Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
			Course("BBB", "Intro to Cheese Grating", "5",
				   "How to grate cheese and avoid grating yourself"),
			Course("CCC", "Cupcakes of Doom", "6",
				   "Just because it's a small cake doesn't mean it's not loaded "
				   "with sugar"),
			Course("CCC", "Cakes and You", "6",
				   "The evolution of Cakes over the history of humankind") };

		vector<Course> expectedResult = {
			Course("AAA", "Intro to Potato Peeling", "3",
				   "How to peel potatoes and avoid peeling yourself"),
			Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
		};

		vector<Course> actualResult;

		string searchInput = "AAA";

		cout << endl << "TEST 1: SEARCH BY DEPARTMENT" << endl;
		cout << "Test set of courses: " << endl;
		DisplayCourseTable(courseList);
		cout << endl << "Search term: \"" << searchInput << "\"" << endl;

		cout << endl << "SearchCoursesByDepartment() call..." << endl;
		actualResult = SearchCoursesByDepartment(courseList, searchInput);

		cout << endl << "EXPECTED RESULT:" << endl;
		DisplayCourseTable(expectedResult);

		cout << endl << "ACTUAL RESULT: " << endl;
		DisplayCourseTable(actualResult);

		// Check for matches
		bool allMatch = true;
		for (unsigned int i = 0; i < expectedResult.size(); i++) {
			if (i >= actualResult.size()) {
				allMatch = false;
			}
			else if (expectedResult[i] != actualResult[i]) {
				allMatch = false;
			}
		}

		if (allMatch) {
			cout << endl << "EVERYTHING MATCHES - TEST PASSES" << endl;
		}
		else {
			cout << endl << "NOT EVERYTHING MATCHES - TEST FAILS" << endl;
		}
	}

	cout << endl << string(80 * 3, '*') << endl;

	{ // Search by title test
		vector<Course> courseList = {
			Course("AAA", "Intro to Potato Peeling", "3",
				   "How to peel potatoes and avoid peeling yourself"),
			Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
			Course("BBB", "Intro to Cheese Grating", "5",
				   "How to grate cheese and avoid grating yourself"),
			Course("CCC", "Cupcakes of Doom", "6",
				   "Just because it's a small cake doesn't mean it's not loaded "
				   "with sugar"),
			Course("CCC", "Cakes and You", "6",
				   "The evolution of Cakes over the history of humankind") };

		vector<Course> expectedResult = {
			Course("AAA", "Intro to Potato Peeling", "3",
				   "How to peel potatoes and avoid peeling yourself"),
			Course("BBB", "Intro to Cheese Grating", "5",
				   "How to grate cheese and avoid grating yourself"),
		};

		vector<Course> actualResult;

		string searchInput = "Intro";

		cout << endl << "TEST 2: SEARCH BY TITLE" << endl;
		cout << "Test set of courses: " << endl;
		DisplayCourseTable(courseList);
		cout << endl << "Search term: \"" << searchInput << "\"" << endl;

		cout << endl << "SearchCoursesByTitle() call..." << endl;
		actualResult = SearchCoursesByTitle(courseList, searchInput);

		cout << endl << "EXPECTED RESULT:" << endl;
		DisplayCourseTable(expectedResult);

		cout << endl << "ACTUAL RESULT: " << endl;
		DisplayCourseTable(actualResult);

		// Check for matches
		bool allMatch = true;
		for (unsigned int i = 0; i < expectedResult.size(); i++) {
			if (i >= actualResult.size()) {
				allMatch = false;
			}
			else if (expectedResult[i] != actualResult[i]) {
				allMatch = false;
			}
		}

		if (allMatch) {
			cout << endl << "EVERYTHING MATCHES - TEST PASSES" << endl;
		}
		else {
			cout << endl << "NOT EVERYTHING MATCHES - TEST FAILS" << endl;
		}
	}

	cout << endl << string(80 * 3, '*') << endl;

	{ // Search by hours test
		vector<Course> courseList = {
			Course("AAA", "Intro to Potato Peeling", "3",
				   "How to peel potatoes and avoid peeling yourself"),
			Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
			Course("BBB", "Intro to Cheese Grating", "5",
				   "How to grate cheese and avoid grating yourself"),
			Course("CCC", "Cupcakes of Doom", "6",
				   "Just because it's a small cake doesn't mean it's not loaded "
				   "with sugar"),
			Course("CCC", "Cakes and You", "6",
				   "The evolution of Cakes over the history of humankind") };

		vector<Course> expectedResult = {
			Course("CCC", "Cupcakes of Doom", "6",
				   "Just because it's a small cake doesn't mean it's not loaded "
				   "with sugar"),
			Course("CCC", "Cakes and You", "6",
				   "The evolution of Cakes over the history of humankind") };

		vector<Course> actualResult;

		string searchInput = "6";

		cout << endl << "TEST 3: SEARCH BY HOURS" << endl;
		cout << "Test set of courses: " << endl;
		DisplayCourseTable(courseList);
		cout << endl << "Search term: \"" << searchInput << "\"" << endl;

		cout << endl << "SearchCoursesByHours() call..." << endl;
		actualResult = SearchCoursesByHours(courseList, searchInput);

		cout << endl << "EXPECTED RESULT:" << endl;
		DisplayCourseTable(expectedResult);

		cout << endl << "ACTUAL RESULT: " << endl;
		DisplayCourseTable(actualResult);

		// Check for matches
		bool allMatch = true;
		for (unsigned int i = 0; i < expectedResult.size(); i++) {
			if (i >= actualResult.size()) {
				allMatch = false;
			}
			else if (expectedResult[i] != actualResult[i]) {
				allMatch = false;
			}
		}

		if (allMatch) {
			cout << endl << "EVERYTHING MATCHES - TEST PASSES" << endl;
		}
		else {
			cout << endl << "NOT EVERYTHING MATCHES - TEST FAILS" << endl;
		}
	}

	cout << endl << string(80 * 3, '*') << endl;

	{ // Search by description test

		vector<Course> courseList = {
			Course("AAA", "Intro to Potato Peeling", "3",
				   "How to peel potatoes and avoid peeling yourself"),
			Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
			Course("BBB", "Intro to Cheese Grating", "5",
				   "How to grate cheese and avoid grating yourself"),
			Course("CCC", "Cupcakes of Doom", "6",
				   "Just because it's a small cake doesn't mean it's not loaded "
				   "with sugar"),
			Course("CCC", "Cakes and You", "6",
				   "The evolution of Cakes over the history of humankind") };

		vector<Course> expectedResult = {
			Course("CCC", "Cupcakes of Doom", "6",
				   "Just because it's a small cake doesn't mean it's not loaded "
				   "with sugar"),
			Course("CCC", "Cakes and You", "6",
				   "The evolution of Cakes over the history of humankind") };

		vector<Course> actualResult;

		string searchInput = "cake";

		cout << endl << "TEST 4: SEARCH BY DESCRIPTION" << endl;
		cout << "Test set of courses: " << endl;
		DisplayCourseTable(courseList);
		cout << endl << "Search term: \"" << searchInput << "\"" << endl;

		cout << endl << "SearchCoursesByDescription() call..." << endl;
		actualResult = SearchCoursesByDescription(courseList, searchInput);

		cout << endl << "EXPECTED RESULT:" << endl;
		DisplayCourseTable(expectedResult);

		cout << endl << "ACTUAL RESULT: " << endl;
		DisplayCourseTable(actualResult);

		// Check for matches
		bool allMatch = true;
		for (unsigned int i = 0; i < expectedResult.size(); i++) {
			if (i >= actualResult.size()) {
				allMatch = false;
			}
			else if (expectedResult[i] != actualResult[i]) {
				allMatch = false;
			}
		}

		if (allMatch) {
			cout << endl << "EVERYTHING MATCHES - TEST PASSES" << endl;
		}
		else {
			cout << endl << "NOT EVERYTHING MATCHES - TEST FAILS" << endl;
		}
	}

	cout << endl << string(80 * 3, '*') << endl;

	{ // Bubble sort / title test
		vector<Course> courseList = {
			Course("asdf", "TitleB", "3", "Lorem ipsum"),
			Course("zxcv", "TitleZ", "4", "Dolor sit amet"),
			Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
			Course("rtyu", "TitleX", "6", "Elit sed do") };

		vector<Course> expectedResult = {
			Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
			Course("asdf", "TitleB", "3", "Lorem ipsum"),
			Course("rtyu", "TitleX", "6", "Elit sed do"),
			Course("zxcv", "TitleZ", "4", "Dolor sit amet") };

		vector<Course> actualResult;

		cout << endl << "TEST 1: BUBBLE SORT / SORT BY TITLE TEST" << endl;
		cout << "Course list before sort: " << endl;
		DisplayCourseTable(courseList);

		cout << endl << "BubbleSortCoursesByTitle() call..." << endl;
		actualResult = BubbleSortCoursesByTitle(courseList);

		cout << endl << "EXPECTED RESULT:" << endl;
		DisplayCourseTable(expectedResult);

		cout << endl << "ACTUAL RESULT: " << endl;
		DisplayCourseTable(actualResult);

		// Check for matches
		bool allMatch = true;
		for (unsigned int i = 0; i < expectedResult.size(); i++) {
			if (expectedResult[i] != actualResult[i]) {
				allMatch = false;
			}
		}

		if (allMatch) {
			cout << endl << "EVERYTHING MATCHES - TEST PASSES" << endl;
		}
		else {
			cout << endl << "NOT EVERYTHING MATCHES - TEST FAILS" << endl;
		}
	} // End bubble sort test

	cout << endl << string(80 * 3, '*') << endl;

	{ // Insertion sort /  test
		vector<Course> courseList = {
			Course("asdf", "TitleB", "3", "Lorem ipsum"),
			Course("zxcv", "TitleZ", "4", "Dolor sit amet"),
			Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
			Course("rtyu", "TitleX", "6", "Elit sed do") };

		vector<Course> expectedResult = {
			Course("asdf", "TitleB", "3", "Lorem ipsum"),
			Course("zxcv", "TitleZ", "4", "Dolor sit amet"),
			Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
			Course("rtyu", "TitleX", "6", "Elit sed do") };

		vector<Course> actualResult;

		cout << endl << "TEST 1: INSERTION SORT / SORT BY HOURS TEST" << endl;
		cout << "Course list before sort: " << endl;
		DisplayCourseTable(courseList);

		cout << endl << "InsertionSortCoursesByHours() call..." << endl;
		actualResult = InsertionSortCoursesByHours(courseList);

		cout << endl << "EXPECTED RESULT:" << endl;
		DisplayCourseTable(expectedResult);

		cout << endl << "ACTUAL RESULT: " << endl;
		DisplayCourseTable(actualResult);

		// Check for matches
		bool allMatch = true;
		for (unsigned int i = 0; i < expectedResult.size(); i++) {
			if (expectedResult[i] != actualResult[i]) {
				allMatch = false;
			}
		}

		if (allMatch) {
			cout << endl << "EVERYTHING MATCHES - TEST PASSES" << endl;
		}
		else {
			cout << endl << "NOT EVERYTHING MATCHES - TEST FAILS" << endl;
		}
	} // End bubble sort test
}

unsigned int CourseManager::GetCourseCount() { return m_courses.size(); }

vector<Course> CourseManager::GetCourses() { return m_courses; }
