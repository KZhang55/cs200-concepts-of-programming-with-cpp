#ifndef _QUESTION_HPP
#define _QUESTION_HPP

#include <iostream>
#include <string>
using namespace std;

class Question
{
    public:
    void SetQuestion( string question );
    void DisplayQuestion();

    protected:
    string m_question;
};



#endif
