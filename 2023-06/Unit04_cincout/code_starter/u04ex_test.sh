#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 purchase"
g++ program1/purchase.cpp -o p1purchase
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 pizza"
g++ program1/pizza.cpp -o p1pizza
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 size"
g++ program1/size.cpp -o p1size
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 student"
g++ program2/student.cpp -o p2student
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 product"
g++ program2/product.cpp -o p2product
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 pet"
g++ program2/pet.cpp -o p2pet
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


echo "====================================================== PROGRAM 1 ==="
# ---------------------------------------------------------------------- TEST A-1
echo -e "\n TEST A-1: ./p1purchase (MANUAL CHECK) ----------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Product: Bean Burrito"
echo -e "Price:   \$1.99"
echo -e "How many would you like to purchase? 3"
echo -e "You entered: 3"
echo -e "Total cost: \$5.97"
echo -e "\nACTUAL OUTPUT:"
echo 3 | ./p1purchase
 
# ---------------------------------------------------------------------- TEST A-2
echo -e "\n TEST A-2: ./p1purchase (MANUAL CHECK) ----------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Product: Bean Burrito"
echo -e "Price:   \$1.99"
echo -e "How many would you like to purchase? 5"
echo -e "You entered: 5"
echo -e "Total cost: \$9.95"
echo -e "\nACTUAL OUTPUT:"
echo 5 | ./p1purchase

# ---------------------------------------------------------------------- TEST B-1
echo -e "\n TEST B-1: ./p1pizza (MANUAL CHECK) -------------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Product: There are 8 servings of Pizza"
echo -e "How many people are there?"
echo -e "You entered: 4"
echo -e "Servings per 1 person: 2"
echo -e "\nACTUAL OUTPUT:"
echo 4 | ./p1pizza

# ---------------------------------------------------------------------- TEST B-2
echo -e "\n TEST B-2: ./p1pizza (MANUAL CHECK) -------------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Product: There are 8 servings of Pizza"
echo -e "How many people are there?"
echo -e "You entered: 5"
echo -e "Servings per 1 person: 1.6"
echo -e "\nACTUAL OUTPUT:"
echo 5 | ./p1pizza

# ---------------------------------------------------------------------- TEST C-1
echo -e "\n TEST C-1: ./p1size (MANUAL CHECK) ----------------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Enter an amount of kilobytes: 2"
echo -e "You entered: 2"
echo -e "Total bytes: 2048"
echo -e "\nACTUAL OUTPUT:"
echo 2 | ./p1size

# ---------------------------------------------------------------------- TEST C-2
echo -e "\n TEST C-2: ./p1pizza (MANUAL CHECK) -------------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Enter an amount of kilobytes: 10"
echo -e "You entered: 10"
echo -e "Total bytes: 10240"
echo -e "\nACTUAL OUTPUT:"
echo 10 | ./p1size

# ---------------------------------------------------------------------- TEST D-1
echo -e "\n TEST D-1: ./p2student (MANUAL CHECK) ----------------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Enter student name: (input)"
echo -e "Enter student ID: (input)"
echo -e "Enter degree: (input)"
echo -e "Enter city: (input)"
echo -e "\nACTUAL OUTPUT:"
./p2student

# ---------------------------------------------------------------------- TEST E-1
echo -e "\n TEST E-1: ./p2product (MANUAL CHECK) ----------------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Enter product name: (input)"
echo -e "Enter category: (input)"
echo -e "Enter subcategory: (input)"
echo -e "\nACTUAL OUTPUT:"
./p2product

# ---------------------------------------------------------------------- TEST F-1
echo -e "\n TEST F-1: ./p2pet (MANUAL CHECK) ----------------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Enter pet name: (input)"
echo -e "Enter age: (input)"
echo -e "Enter animal type: (input)"
echo -e "Enter breed: (input)"
echo -e "\nACTUAL OUTPUT:"
./p2student
