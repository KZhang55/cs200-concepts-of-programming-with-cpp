#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/* STUDENT NAME:
 * Create four variables: name (string), price (float), category (string), subcategory (string).
 * 
 * Ask the user to enter the name. Use `getline( cin, name );`.
 * Ask the user to enter the price. Use `cin >> price;`.
 * Use `cin.ignore();` to prepare for the next input.
 * Ask the user to enter the category. Use `getline( cin, category );`.
 * Ask the user to enter the subcategory. Use `getline( cin, subcategory );`.
 * 
 * Use `cout << left;` to set up left-aligned text.
 * Display "Collected information".
 * Use `cout << setw( 15 ) << "Name: " << setw( 10 ) << name << endl;`
      to display the name in a table.
 * Do the same to display the remaining information.
 * */

int main()
{
  
  return 0;
}
