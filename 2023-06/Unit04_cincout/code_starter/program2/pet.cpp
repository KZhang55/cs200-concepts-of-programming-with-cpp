#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/* STUDENT NAME:
 * Create four variables: name (string), age (int), animal (string), breed (string).
 * 
 * Ask the user to enter the name. Use `getline( cin, name );`.
 * Ask the user to enter the age. Use `cin >> age;`.
 * Use `cin.ignore();` to prepare for the next input.
 * Ask the user to enter the animal. Use `getline( cin, animal );`.
 * Ask the user to enter the breed. Use `getline( cin, breed );`.
 * 
 * Use `cout << left;` to set up left-aligned text.
 * Display "Collected information".
 * Use `cout << setw( 15 ) << "Name: " << setw( 10 ) << name << endl;`
      to display the name in a table.
 * Do the same to display the remaining information.
 * */

int main()
{
  
  return 0;
}
