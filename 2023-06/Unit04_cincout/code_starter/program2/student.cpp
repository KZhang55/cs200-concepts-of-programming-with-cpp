#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/* STUDENT NAME:
 * Create four variables: name (string), studentId (int), degree (string), city (string).
 * 
 * Ask the user to enter the name. Use `getline( cin, name );`.
 * Ask the user to enter the studentId. Use `cin >> studentId;`.
 * Use `cin.ignore();` to prepare for the next input.
 * Ask the user to enter the degree. Use `getline( cin, degree );`.
 * Ask the user to enter the city. Use `getline( cin, city );`.
 * 
 * Use `cout << left;` to set up left-aligned text.
 * Display "Collected information".
 * Use `cout << setw( 15 ) << "Name: " << setw( 10 ) << name << endl;`
      to display the name in a table.
 * Do the same to display the remaining information.
 * */

int main()
{
  
  return 0;
}
