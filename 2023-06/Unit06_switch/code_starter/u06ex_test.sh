#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 translator.exe"
g++ program1/translator.cpp -o translator.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 1 neighbor.exe"
g++ program1/neighbor.cpp -o neighbor.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 1 movie.exe"
g++ program1/movie.cpp -o movie.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


echo "====================================================== PROGRAM 1 ==="
# ---------------------------------------------------------------------- TRANSLATOR PROGRAM
# ---------------------------------------------------------------------- TEST A-1
echo -e "\n TEST A-1: ./translator.exe e -----------------------------------------------"
EXPECTED="cat = kato"
ACTUAL=`./translator.exe e`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST A-2
echo -e "\n TEST A-2: ./translator.exe s -----------------------------------------------"
EXPECTED="cat = gato"
ACTUAL=`./translator.exe s`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST A-3
echo -e "\n TEST A-3: ./translator.exe s -----------------------------------------------"
EXPECTED="cat = mao"
ACTUAL=`./translator.exe m`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST A-4
echo -e "\n TEST A-4: ./translator.exe s -----------------------------------------------"
EXPECTED="cat = billee"
ACTUAL=`./translator.exe h`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST A-5
echo -e "\n TEST A-5: ./translator.exe s -----------------------------------------------"
EXPECTED="cat = ?"
ACTUAL=`./translator.exe x`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- NEIGHBOR PROGRAM
# ---------------------------------------------------------------------- TEST B-1
echo -e "\n TEST B-1: ./neighbor.exe n -----------------------------------------------"
EXPECTED="North of Kansas is Nebraska"
ACTUAL=`./neighbor.exe n`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST B-2
echo -e "\n TEST B-2: ./neighbor.exe s -----------------------------------------------"
EXPECTED="South of Kansas is Oklahoma"
ACTUAL=`./neighbor.exe s`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST B-3
echo -e "\n TEST B-3: ./neighbor.exe e -----------------------------------------------"
EXPECTED="East of Kansas is Missouri"
ACTUAL=`./neighbor.exe e`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST B-4
echo -e "\n TEST B-4: ./neighbor.exe w -----------------------------------------------"
EXPECTED="West of Kansas is Colorado"
ACTUAL=`./neighbor.exe w`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST B-5
echo -e "\n TEST B-5: ./neighbor.exe x -----------------------------------------------"
EXPECTED="? of Kansas is ?"
ACTUAL=`./neighbor.exe x`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi


# ---------------------------------------------------------------------- MOVIE PROGRAM
# ---------------------------------------------------------------------- TEST B-1
echo -e "\n TEST C-1: ./movie.exe a -----------------------------------------------"
EXPECTED="Recommended action movie: Die Hard"
ACTUAL=`./movie.exe a`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-2
echo -e "\n TEST C-2: ./movie.exe c -----------------------------------------------"
EXPECTED="Recommended comedy movie: Bowfinger"
ACTUAL=`./movie.exe c`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-3
echo -e "\n TEST C-3: ./movie.exe r -----------------------------------------------"
EXPECTED="Recommended romance movie: You've Got Mail"
ACTUAL=`./movie.exe r`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-4
echo -e "\n TEST C-4: ./movie.exe d -----------------------------------------------"
EXPECTED="Recommended documentary movie: Coded Bias"
ACTUAL=`./movie.exe d`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-5
echo -e "\n TEST C-5: ./movie.exe x -----------------------------------------------"
EXPECTED="Recommended ? movie: ?"
ACTUAL=`./movie.exe x`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
