#include <iostream>
#include <string>
using namespace std;

/* STUDENT NAME:
 * Get args[1][0] and store it as a char variable - the direction.
 * Create a string for state and assign it "Kansas".
 * Create a string for the direction text.
 * Create a string for the neighbor state.
 * Use a switch statement to assign a value to the neighbor variable, based on the direction:
 * direction is 'n': then store "Nebraska" in the neighbor, store "North" in the direction text.
 * direction is 's': then store "Oklahoma" in the neighbor, store "South" in the direction text.
 * direction is 'e': then store "Missouri" in the neighbor, store "East" in the direction text.
 * direction is 'w': then store "Colorado" in the neighbor, store "West" in the direction text.
 * otherwise: store "?" in the neighbor variable, store "?" in the direction text.
 * Afterwards, display a message in the form "North of Kansas is Nebraska".
 * */
 
int main(int argCount, char *args[]) {
  if (argCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS! Expected: direction" << endl;
    cout << "n = north, s = south, e = east, w = west" << endl;
    return 1;
  }

  // Add code here

  return 0;
}
