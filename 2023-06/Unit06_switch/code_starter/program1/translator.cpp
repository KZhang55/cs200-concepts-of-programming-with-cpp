#include <iostream>
#include <string>
using namespace std;

/* STUDENT NAME:
 * Get args[1][0] and store it as a char variable - the language code.
 * Create a string for english and assign it "cat".
 * Create a string for the translated text.
 * Use a switch statement to assign a value to the translate variable, based on the language code:
 * language code is 'e': then store "kato" in the translate variable.
 * language code is 's': then store "gato" in the translate variable.
 * language code is 'm': then store "mao" in the translate variable.
 * language code is 'h': then store "billee" in the translate variable.
 * otherwise: store "?" in the translate variable.
 * Afterwards, display english = translated.
 * */
 
int main(int argCount, char *args[]) {
  if (argCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS! Expected: language" << endl;
    cout << "e = esperanto, s = spanish, m = mandarin, h = hindi" << endl;
    return 1;
  }

  // Add code here

  return 0;
}
