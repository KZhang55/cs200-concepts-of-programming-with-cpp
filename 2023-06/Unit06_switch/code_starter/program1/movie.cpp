#include <iostream>
#include <string>
using namespace std;

/* STUDENT NAME:
 * Get args[1][0] and store it as a char variable - the genre.
 * Create a string for the genre name.
 * Create a string for the suggested movie.
 * Use a switch statement to assign a value to the genre name and suggested movie variables, based on the genre char:
 * genre is 'a': then store "action" in the genre name, store "Die Hard" in the suggested movie.
 * genre is 'c': then store "comedy" in the genre name, store "Bowfinger" in the suggested movie.
 * genre is 'r': then store "romance" in the genre name, store "You've Got Mail"  in the suggested movie.
 * genre is 'd': then store "documentary" in the genre name, store "Coded Bias"  in the suggested movie.
 * otherwise: store "?" in the genre name, store "?" in the suggested movie.
 * Afterwards, display a message in the form "Recommended action movie: Die Hard"
 * */
 
int main(int argCount, char *args[]) {
  if (argCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS! Expected: genre" << endl;
    cout << "a = action, c = comedy, r = romance, d = documentary" << endl;
    return 1;
  }

  // Add code here

  return 0;
}
