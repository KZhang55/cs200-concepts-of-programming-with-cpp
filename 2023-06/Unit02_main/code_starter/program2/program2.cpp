#include <iostream>  // able to use cout to display text
using namespace std; // use the standard C++ library

int main(int argCount, char* args[])
{
  cout << "There are " << argCount << " arguments" << endl;
  
  // Display your team's name

  // Each student write 4 cout statements to
  // introduce themselves, such as:
  // name, favorite color, food, and hobbies.

  // STUDENT A -------------------------------

  // STUDENT B -------------------------------

  // STUDENT C -------------------------------
  
  return 0; // end program with no errors
}