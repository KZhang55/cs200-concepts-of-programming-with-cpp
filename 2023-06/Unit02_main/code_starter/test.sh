#! /bin/bash
# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

clear
echo "=== BUILD PROGRAMS ==="


# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1"
g++ program1/*.cpp -o program1exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2"
g++ program2/*.cpp -o program2exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


# ---------------------------------------------------------------------- RUN PROGRAM 1
echo "====================================================== PROGRAM 1 ==="
echo "EXPECTATION: Should display message like"
echo "TEAM [NAME]"
echo ""
echo "Hi, I'm NAME!"
echo "My favorite color is COLOR"
echo "My favorite food is FOOD"
echo "Some of my hobbies are HOBBY1, HOBBY2!"
echo ""
echo "With introductions for each team member"
echo ""
echo "PROGRAM RUN: ./program1exe"
./program1exe

# ---------------------------------------------------------------------- RUN PROGRAM 2
echo "====================================================== PROGRAM 2 ==="
echo "EXPECTATION: Should take in arguments and display a message like the previous program"
echo ""
echo "PROGRAM RUN: ./program2exe A B C"
./program2exe A B C
echo ""
echo "PROGRAM RUN: ./program2exe Hikaru Umi Fuu"
./program2exe Hikaru Umi Fuu
