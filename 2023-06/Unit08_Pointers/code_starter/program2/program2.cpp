#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

void DisplayTable( float& price1, float& price2, float& price3 )
{
  cout << left;
  cout << setw( 20 ) << "VARIABLE"
       << setw( 20 ) << "VALUE"
       << setw( 20 ) << "ADDRESS"
       << endl << string( 60, '-' ) << endl;
  
  cout << setw( 20 ) << "price1"
       << setw( 20 ) << price1
       << setw( 20 ) << &price1 << endl;
  
  cout << setw( 20 ) << "price2"
       << setw( 20 ) << price2
       << setw( 20 ) << &price2 << endl;
  
  cout << setw( 20 ) << "price3"
       << setw( 20 ) << price3
       << setw( 20 ) << &price3 << endl;
  cout << endl;
}

int main()
{
  cout << "POINTING TO VARIABLE ADDRESSES" << endl << endl;
  
  // 1. Create float variables and assign values
  
  // 2. Create a float pointer and initialize it to nullptr
  
  // 3. Display table of variable values
  
  // 4. Ask the user which variable to point to.

  // 5. Assign ptr to an address based on user's selection
  
  // 6. Display the address and value via the ptr
  
  // 7. Ask the user to enter a new value for this item, assign via the ptr.

  
  // 8. Display updated table
  
  return 0;
}
