#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create two float variables, num1 and num2.
 * Convert argument1 and argument2 to floats and store in those variables.
 * Create a third float variable, sum. Store the result of num1 + num2 in sum.
 * Display the result like this:
 * A+B=C
 * Where A is num1, B is num2, and C is the sum.
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "Not enough arguments!" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
