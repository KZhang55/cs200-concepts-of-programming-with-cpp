#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create four float variables: x1, y1, x2, and y2.
 * Convert the arguments to floats and store in the variables:
 *    x1 is args[1]     y1 is args[2]
 *    x2 is args[3]     y2 is args[4]
 * Create a float variable: slope
 * Calculate the slope with (y2-y1)/(x2-x1).
 * Display the result like this:
 * slope=X
 * Where X is the slope.
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 5 ) { cout << "Not enough arguments! Give: x1 y1 x2 y2" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
