#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create two float variables: width and length.
 * Convert the arguments to floats and store in the variables:
 *    width is args[1]     length is args[2]
 * Create a float variable: perimeter
 * Calculate the value of perimeter as 2*width+2*length.
 * Display the result like this:
 * perimeter=X
 * Where X is the perimeter value.
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "Not enough arguments! Give: width length" << endl; return 1; }

  // Add code here
  
  return 0;
}
