#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create two float variables: a and b.
 * Convert the arguments to floats and store in the variables:
 *    a is args[1]     b is args[2]
 * Create a float variable: csquared
 * Calculate the value of csquared as a*a+b*b.
 * Display the result like this:
 * c^2=X
 * Where X is the csquared value.
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "Not enough arguments! Give: a b." << endl; return 1; }
  
  // Add code here
  
  return 0;
}
