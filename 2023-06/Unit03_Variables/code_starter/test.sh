#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 add"
g++ program1/add.cpp -o p1add
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 sub"
g++ program1/sub.cpp -o p1sub
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 mult"
g++ program1/mult.cpp -o p1mult
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 dimensions"
g++ program2/dimensions.cpp -o p2dimensions
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 pythagorean"
g++ program2/pythagorean.cpp -o p2pythagorean
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 slope"
g++ program2/slope.cpp -o p2slope
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

echo "====================================================== PROGRAM 1 ==="
# ---------------------------------------------------------------------- TEST A-1
echo -e "\n TEST A-1: ./p1add 3 4 (EXPECTING RESULT)"
EXPECTED="3+4=7"
ACTUAL=`./p1add 3 4`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST A-2
echo -e "\n TEST A-2: ./p1add 2.5 7.1 (EXPECTING RESULT)"
EXPECTED="2.5+7.1=9.6"
ACTUAL=`./p1add 2.5 7.1`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST B-1
echo -e "\n TEST B-1: ./p1sub 3 4 (EXPECTING RESULT)"
EXPECTED="3-4=-1"
ACTUAL=`./p1sub 3 4`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST B-2
echo -e "\n TEST B-2: ./p1sub 3 4 (EXPECTING RESULT)"
EXPECTED="5.5-1.1=4.4"
ACTUAL=`./p1sub 5.5 1.1`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST C-1
echo -e "\n TEST C-1: ./p1mult 3 4 (EXPECTING RESULT)"
EXPECTED="3*4=12"
ACTUAL=`./p1mult 3 4`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST C-2
echo -e "\n TEST C-2: ./p1mult 5 0.5 (EXPECTING RESULT)"
EXPECTED="5*0.5=2.5"
ACTUAL=`./p1mult 5 0.5`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

echo "====================================================== PROGRAM 2 ==="

# ---------------------------------------------------------------------- TEST D-1
echo -e "\n TEST D-1: ./p2dimensions 3 4 (EXPECTING RESULT)"
EXPECTED="perimeter=14"
ACTUAL=`./p2dimensions 3 4`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST D-2
echo -e "\n TEST D-2: ./p2dimensions 10 5 (EXPECTING RESULT)"
EXPECTED="perimeter=30"
ACTUAL=`./p2dimensions 10 5`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST E-1
echo -e "\n TEST E-1: ./p2pythagorean 3 4 (EXPECTING RESULT)"
EXPECTED="c^2=25"
ACTUAL=`./p2pythagorean 3 4`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST E-2
echo -e "\n TEST E-2: ./p2pythagorean 10 5 (EXPECTING RESULT)"
EXPECTED="c^2=29"
ACTUAL=`./p2pythagorean 2 5`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST F-1
echo -e "\n TEST E-1: ./p2slope 3 4 (EXPECTING RESULT)"
EXPECTED="slope=-0.25"
ACTUAL=`./p2slope 4 2 8 1`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST F-2
echo -e "\n TEST E-2: ./p2slope 10 5 (EXPECTING RESULT)"
EXPECTED="slope=2"
ACTUAL=`./p2slope 6 3 9 9`
if [ $ACTUAL == $EXPECTED ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
