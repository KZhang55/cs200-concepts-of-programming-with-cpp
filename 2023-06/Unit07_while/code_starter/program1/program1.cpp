#include <iostream>
using namespace std;

int main( int argCount, char* args[] )
{
  // Error check: Enough arguments?
  if ( argCount < 3 ) 
  {
    cout << "NOT ENOUGH ARGUMENTS! Expected form: " << endl;
    cout << args[0] << " low high" << endl;
    return 1;
  }
  
  int low = stoi( args[1] );
  int high = stoi( args[2] );
  int counter;
  
  // STUDENT NAME: 
  // Use a while loop to count from (low) to (high), going up by 2 each time.
  // Example: 2 4 6 8 10

  // Enter code here

  
  // STUDENT NAME:
  // Use a while loop to count from (high) to (low), going down by 2 each time.
  // Example: 20 18 16 14

  // Enter code here

  
  // STUDENT NAME:
  // Use a while loop to count from (low) to (high), multiplying the counter by 2 each time.
  // Example: 2 4 8 16 32

  // Enter code here

  
  return 0;
}
