#include <iostream>
#include <iomanip>
using namespace std;

/**
 * STUDENT NAME:
    Program arguments:  `basepay` (float), from args[1],
                        `raise` (float), from args[2],
                        `years` (int), from args[3]
                        
    1. Load the arguments into their variables.
    2. Create a pay float variable, initialize it to the basepay.
    3. Create a counter integer variable, initialize it to 1.
    4. While the counter is less than or equal to the years, do the following:
        a) Create a increase float variable. To calculate how much the pay will increase by, multiply pay by (raise/100). Store it in this variable.
        b) Display “year: ” counter “, pay: $” pay “, increase: $” increase.
        c) Add the increase to the pay, storing it in the pay variable.
        d) Increment counter by 1.
    5. After the while loop, display “year: ” counter “, pay: $” pay.
 * */

int main( int argCount, char* args[] ) 
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: basepay raisepercent yearcount" << endl; return 1; }
  
  // Enter code here

  return 0;
}
