#include <iostream>
#include <iomanip>
using namespace std;

/**
 * STUDENT NAME:
 
    Program arguments: `n` (int), from args[1]
 
    1. Load the arguments into their variables.
    2. Set up USD formatting: cout << fixed << setprecision( 2 );
    3. Create a product integer variable and initialize it to 1.
    4. Create a counter integer variable and initialize it to n.
    5. Display n “!=”
    6. While the counter is greater than 0, do the following:
        a) If the counter is not equal to n, then display “*”.
        b) Display the counter.
        c) Multiply the product by the counter and store the result back in product.
        d) Decrement counter by 1.
    7. After the while loop, display “=” and product.
 * */

int main( int argCount, char* args[] ) 
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: n" << endl; return 1; }
  
  // Enter code here

  return 0;
}
