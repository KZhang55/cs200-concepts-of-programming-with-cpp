#include <iostream>
#include <iomanip>
using namespace std;

/**
 * STUDENT NAME:
 * 
    Program arguments:  `low` (int), from args[1],
                        `high` (int), from args[2]
 
    1. Load the arguments into their variables.
    2. Create a sum integer variable. Initialize it to 0.
    3. Create a counter integer variable. Initialize it to the low value.
    4. While the counter is less than or equal to the high value, do the following:
        a) Display sum “+” counter “=”
        b) Add counter to sum and store the result in sum.
        c) Display sum.
        d) Increment counter by 1.
 * */

int main( int argCount, char* args[] ) 
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: low high" << endl; return 1; }
  
  // Enter code here

  return 0;
}
