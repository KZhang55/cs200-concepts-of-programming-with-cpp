#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 program1.exe"
g++ program1/program1.cpp -o program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 program2.exe"
g++ program2/program2.cpp -o program2.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM 3
NAME="program 3 p3inrange.exe"
g++ program3/inrange.cpp -o p3inrange.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM 3
NAME="program 3 p3outrange.exe"
g++ program3/outrange.cpp -o p3outrange.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM 3
NAME="program 3 p3options.exe"
g++ program3/options.cpp -o p3options.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 4
NAME="program 4 raise"
g++ program4/raise.cpp -o p4raise.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM 4
NAME="program 4 sum"
g++ program4/sum.cpp -o p4sum.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM 4
NAME="program 4 factorial"
g++ program4/factorial.cpp -o p4factorial.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

echo "====================================================== PROGRAM 1 ==="
# ---------------------------------------------------------------------- COUNTING WITH LOOPS
# ---------------------------------------------------------------------- TEST A-1
echo -e "\n TEST A-1: ./program1.exe 2 10 -----------------------------------------------"
EXPECTED="2 4 6 8 10 12 14 16 
16 14 12 10 8 6 4 2 
2 4 8 16 "
ACTUAL=`./program1.exe 2 16`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST B-1
echo -e "\n TEST A-1: ./program1.exe 4 32 -----------------------------------------------"
EXPECTED="4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 
32 30 28 26 24 22 20 18 16 14 12 10 8 6 4 
4 8 16 32 "
ACTUAL=`./program1.exe 4 32`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

echo "====================================================== PROGRAM 2 ==="
# ---------------------------------------------------------------------- KEEP PROGRAM RUNNING
# ---------------------------------------------------------------------- TEST B-1
echo -e "\n TEST B-1: ./program2.exe (MANUAL CHECK) ----------------------------------------------"
echo -e "\nEXPECTED OUTPUT:"
echo -e "Display menu, get user's input, display one of the students' favorite books."
echo -e "If the EXIT option is selected, exit the program."
echo -e "\nACTUAL OUTPUT:"
echo 1 2 3 4 | ./program2.exe

echo "====================================================== PROGRAM 3 ==="
# ---------------------------------------------------------------------- RANGE PROGRAM - INRANGE
# ---------------------------------------------------------------------- TEST C-1
echo -e "\n TEST C-1: ./p3inrange.exe 1 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 1 BETWEEN"
ACTUAL=`./p3inrange.exe 1`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-2
echo -e "\n TEST C-2: ./p3inrange.exe 5 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 5 BETWEEN"
ACTUAL=`./p3inrange.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-3
echo -e "\n TEST C-3: ./p3inrange.exe 10 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 10 BETWEEN"
ACTUAL=`./p3inrange.exe 10`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-4
echo -e "\n TEST C-4: ./p3inrange.exe 1 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 0 OUTSIDE"
ACTUAL=`./p3inrange.exe 0`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST C-5
echo -e "\n TEST C-5: ./p3inrange.exe 11 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 11 OUTSIDE"
ACTUAL=`./p3inrange.exe 11`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- RANGE PROGRAM - OUTRANGE
# ---------------------------------------------------------------------- TEST D-1
echo -e "\n TEST D-1: ./p3outrange.exe 1 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 1 BETWEEN"
ACTUAL=`./p3outrange.exe 1`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST D-2
echo -e "\n TEST D-2: ./p3outrange.exe 1 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 1 BETWEEN"
ACTUAL=`./p3outrange.exe 1`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST D-3
echo -e "\n TEST D-3: ./p3outrange.exe 5 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 5 BETWEEN"
ACTUAL=`./p3outrange.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST D-4
echo -e "\n TEST D-4: ./p3outrange.exe 10 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 10 BETWEEN"
ACTUAL=`./p3outrange.exe 10`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST D-5
echo -e "\n TEST D-5: ./p3outrange.exe 1 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 0 OUTSIDE"
ACTUAL=`./p3outrange.exe 0`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST D-6
echo -e "\n TEST D-6: ./p3outrange.exe 11 -----------------------------------------------"
EXPECTED="min: 1, max: 10, num: 11 OUTSIDE"
ACTUAL=`./p3outrange.exe 11`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- RANGE PROGRAM - OPTIONS
# ---------------------------------------------------------------------- TEST E-1
echo -e "\n TEST E-1: ./p3options.exe A -----------------------------------------------"
EXPECTED="Options: A, B, C. Selection: VALID"
ACTUAL=`./p3options.exe A`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST E-2
echo -e "\n TEST E-2: ./p3options.exe B -----------------------------------------------"
EXPECTED="Options: A, B, C. Selection: VALID"
ACTUAL=`./p3options.exe B`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST E-3
echo -e "\n TEST E-3: ./p3options.exe C -----------------------------------------------"
EXPECTED="Options: A, B, C. Selection: VALID"
ACTUAL=`./p3options.exe C`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST E-4
echo -e "\n TEST E-4: ./p3options.exe D -----------------------------------------------"
EXPECTED="Options: A, B, C. Selection: INVALID"
ACTUAL=`./p3options.exe D`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi


# ---------------------------------------------------------------------- CALCULATE PROGRAM - RAISE
# ---------------------------------------------------------------------- TEST F-1
echo -e "\n TEST F-1: ./p4raise.exe 50000 5 3 -----------------------------------------------"
EXPECTED="year: 1, pay: \$50000.00, increase: \$2500.00
year: 2, pay: \$52500.00, increase: \$2625.00
year: 3, pay: \$55125.00, increase: \$2756.25
year: 4, pay: \$57881.25"
ACTUAL=`./p4raise.exe 50000 5 3`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST F-2
echo -e "\n TEST F-2: ./p4raise.exe 60000 10 4 -----------------------------------------------"
EXPECTED="year: 1, pay: \$60000.00, increase: \$6000.00
year: 2, pay: \$66000.00, increase: \$6600.00
year: 3, pay: \$72600.00, increase: \$7260.00
year: 4, pay: \$79860.00, increase: \$7986.00
year: 5, pay: \$87846.00"
ACTUAL=`./p4raise.exe 60000 10 4`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- CALCULATE PROGRAM - SUM
# ---------------------------------------------------------------------- TEST G-1
echo -e "\n TEST G-1: ./p4sum.exe 2 5 -----------------------------------------------"
EXPECTED="0+2=2
2+3=5
5+4=9
9+5=14"
ACTUAL=`./p4sum.exe 2 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST G-2
echo -e "\n TEST G-2: ./p4sum.exe 10 14 -----------------------------------------------"
EXPECTED="0+10=10
10+11=21
21+12=33
33+13=46
46+14=60"
ACTUAL=`./p4sum.exe 10 14`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- CALCULATE PROGRAM - FACTORIAL
# ---------------------------------------------------------------------- TEST H-1
echo -e "\n TEST H-1: ./p4factorial.exe 5 -----------------------------------------------"
EXPECTED="5!=5*4*3*2*1=120"
ACTUAL=`./p4factorial.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST H-2
echo -e "\n TEST H-2: ./p4factorial.exe 7 -----------------------------------------------"
EXPECTED="7!=7*6*5*4*3*2*1=5040"
ACTUAL=`./p4factorial.exe 7`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
