#include <iostream>
using namespace std;

/**
 * STUDENT NAME:
 * Create a char named `letter`. Set it to args[1][0].
 * Display the message:
 *    "Options: A, B, C. Selection: "
 * 
 * Use an if statement to check if the user's selection (letter) is A, B, or C.
 *    -- you should have three CONDITIONS
 * is letter equal to 'A'?
 * is letter equal to 'B'?
 * is letter equal to 'C'?
 * 
 * Use a logic operator to combine these conditions for the if statement.
 * 
 * If letter is A, B, or C, then display "VALID"
 * Otherwise, display "INVALID".
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: letter" << endl; return 1; }

  // Enter code here

  return 0;
}
