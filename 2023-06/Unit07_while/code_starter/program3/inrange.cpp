#include <iostream>
using namespace std;

/**
 * STUDENT NAME:
 * Create three int variables: set min to 1, max to 10,
 *    and load args[1] as a third `num` variable.
 * Display the message:
 *    min: MIN, max: MAX, num: NUM
 *    inserting the corresponding variables where relevant.
 * 
 * Use an if statement to check if num is between min and max
 *    -- you should have two CONDITIONS
 * is num greater than or equal to min?
 * is num less than or equal to max?
 * 
 * Use a logic operator to combine these conditions for the if statement.
 * 
 * If `num` is between `min` and `max`, then display " BETWEEN".
 * Otherwise, display " OUTSIDE".
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: number" << endl; return 1; }

  // Enter code here

  return 0;
}
