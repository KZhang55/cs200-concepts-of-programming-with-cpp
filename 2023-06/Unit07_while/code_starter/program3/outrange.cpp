#include <iostream>
using namespace std;

/**
 * STUDENT NAME:
 * Create three int variables: set min to 1, max to 10,
 *    and load args[1] as a third `num` variable.
 * Display the message:
 *    min: MIN, max: MAX, num: NUM
 *    inserting the corresponding variables where relevant.
 * 
 * Use an if statement to check if num outside the min/max range
 *    -- you should have two CONDITIONS
 * is num less than min?
 * is num greater than max?
 * 
 * Use a logic operator to combine these conditions for the if statement.
 * 
 * If `num` is outside of `min` and `max`, then display " OUTSIDE".
 * Otherwise, display " BETWEEN".
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: number" << endl; return 1; }

  // Enter code here

  return 0;
}
