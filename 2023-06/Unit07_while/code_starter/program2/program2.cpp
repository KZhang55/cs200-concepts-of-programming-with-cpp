#include <iostream>
using namespace std;

int main()
{
  cout << "1. Display STUDENT1's favorite book" << endl;
  cout << "2. Display STUDENT2's favorite book" << endl;
  cout << "3. Display STUDENT3's favorite book" << endl;
  cout << "4. Exit program" << endl << endl;
  
  int choice;
  bool running = true;
  
  
  // CREATE A WHILE LOOP THAT CONTINUES LOOPING WHILE running IS true.
  // WITHIN THE LOOP, ASK THE USER TO ENTER A choice.
  // IF choice IS 1, display STUDENT1'S favorite book.
  // OR IF choice IS 2, display STUDENT2'S favorite book.
  // OR IF choice IS 3, display STUDENT3'S favorite book.
  // OR IF choice IS 4, THEN SET running TO false.
  // You can use if/else if statements or a switch statement here.
  
  // Enter code here
  
  // End of program
  cout << "Goodbye." << endl;
  
  return 0;
}
