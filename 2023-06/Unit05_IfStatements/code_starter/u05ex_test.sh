#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="program 1 p1a.exe"
g++ program1/p1a.cpp -o p1a.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 2 p1b.exe"
g++ program1/p1b.cpp -o p1b.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 1 p1c.exe"
g++ program1/p1c.cpp -o p1c.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 2
NAME="program 2 p2a.exe"
g++ program2/p2a.cpp -o p2a.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 2 p2b.exe"
g++ program2/p2b.cpp -o p2b.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 2 p2c.exe"
g++ program2/p2c.cpp -o p2c.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 3
NAME="program 3 p3a.exe"
g++ program3/p3a.cpp -o p3a.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 3 p3b.exe"
g++ program3/p3b.cpp -o p3b.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 3 p3c.exe"
g++ program3/p3c.cpp -o p3c.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

# ---------------------------------------------------------------------- BUILD PROGRAM 4
NAME="program 4 p4a.exe"
g++ program4/p4a.cpp -o p4a.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 4 p4b.exe"
g++ program4/p4b.cpp -o p4b.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

NAME="program 4 p4c.exe"
g++ program4/p4c.cpp -o p4c.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi

echo "====================================================== PROGRAM 1 ==="
# ---------------------------------------------------------------------- IF PROGRAM 1
# ---------------------------------------------------------------------- TEST A-1
echo -e "\n TEST A-1: ./p1a.exe 10 10 -----------------------------------------------"
EXPECTED="Score: 100% (PERFECT)"
ACTUAL=`./p1a.exe 10 10`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST A-2
echo -e "\n TEST A-2: ./p1a.exe 4 5 -------------------------------------------------"
EXPECTED="Score: 80%"
ACTUAL=`./p1a.exe 4 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- IF PROGRAM 2
# ---------------------------------------------------------------------- TEST B-1
echo -e "\n TEST B-1: ./p1b.exe 10.50 100 -----------------------------------------------"
EXPECTED="Balance: \$89.50"
ACTUAL=`./p1b.exe 10.50 100`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST B-2
echo -e "\n TEST B-2: ./p1b 21 20 -----------------------------------------------"
EXPECTED="Balance: \$-1.00 (OVERDRAWN)"
ACTUAL=`./p1b.exe 21 20`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- IF PROGRAM 3
# ---------------------------------------------------------------------- TEST C-1
echo -e "\n TEST C-1: ./p1c 3 6 -----------------------------------------------"
EXPECTED="Remaining gas: 50%"
ACTUAL=`./p1c.exe 3 6`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TEST C-2
echo -e "\n TEST C-2: ./p1c 2 20 -----------------------------------------------"
EXPECTED="Remaining gas: 10% (LOW FUEL)"
ACTUAL=`./p1c.exe 2 20`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- IF/ELSE PROGRAM 1
# ---------------------------------------------------------------------- TEST D-1
echo -e "\n TEST D-1: ./p2a.exe 5 -----------------------------------------------"
EXPECTED="POSITIVE"
ACTUAL=`./p2a.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST D-2
echo -e "\n TEST D-2: ./p2a.exe 0 -----------------------------------------------"
EXPECTED="NEGATIVE OR ZERO"
ACTUAL=`./p2a.exe 0`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST D-3
echo -e "\n TEST D-3: ./p2a.exe -5 -----------------------------------------------"
EXPECTED="NEGATIVE OR ZERO"
ACTUAL=`./p2a.exe -5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- IF/ELSE PROGRAM 2
# ---------------------------------------------------------------------- TEST E-1
echo -e "\n TEST E-1: ./p2b.exe 5 -----------------------------------------------"
EXPECTED="POSITIVE OR ZERO"
ACTUAL=`./p2b.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST E-2
echo -e "\n TEST E-2: ./p2b.exe 0 -----------------------------------------------"
EXPECTED="POSITIVE OR ZERO"
ACTUAL=`./p2b.exe 0`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST E-3
echo -e "\n TEST E-3: ./p2b.exe -5 -----------------------------------------------"
EXPECTED="NEGATIVE"
ACTUAL=`./p2b.exe -5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- IF/ELSE PROGRAM 3
# ---------------------------------------------------------------------- TEST F-1
echo -e "\n TEST F-1: ./p2c.exe 5 -----------------------------------------------"
EXPECTED="POSITIVE OR ZERO"
ACTUAL=`./p2c.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST F-2
echo -e "\n TEST F-2: ./p2c.exe 0 -----------------------------------------------"
EXPECTED="POSITIVE OR ZERO"
ACTUAL=`./p2c.exe 0`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST F-3
echo -e "\n TEST F-3: ./p2c.exe -5 -----------------------------------------------"
EXPECTED="NEGATIVE"
ACTUAL=`./p2c.exe -5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- LETTER GRADE PROGRAM
# ---------------------------------------------------------------------- TEST G-1
echo -e "\n TEST G-1: ./p3a.exe 90 -----------------------------------------------"
EXPECTED="Grade: A"
ACTUAL=`./p3a.exe 90`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST G-2
echo -e "\n TEST G-2: ./p3a.exe 80 -----------------------------------------------"
EXPECTED="Grade: B"
ACTUAL=`./p3a.exe 80`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST G-3
echo -e "\n TEST G-3: ./p3a.exe 70 -----------------------------------------------"
EXPECTED="Grade: C"
ACTUAL=`./p3a.exe 70`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST G-4
echo -e "\n TEST G-4: ./p3a.exe 60 -----------------------------------------------"
EXPECTED="Grade: D"
ACTUAL=`./p3a.exe 60`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST G-5
echo -e "\n TEST G-5: ./p3a.exe 59 -----------------------------------------------"
EXPECTED="Grade: F"
ACTUAL=`./p3a.exe 59`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- BATTERY CHARGE PROGRAM
# ---------------------------------------------------------------------- TEST H-1
echo -e "\n TEST H-1: ./p3b.exe 90 -----------------------------------------------"
EXPECTED="Battery: [****]"
ACTUAL=`./p3b.exe 90`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST H-2
echo -e "\n TEST H-2: ./p3b.exe 75 -----------------------------------------------"
EXPECTED="Battery: [***_]"
ACTUAL=`./p3b.exe 75`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST H-3
echo -e "\n TEST H-3: ./p3b.exe 50 -----------------------------------------------"
EXPECTED="Battery: [**__]"
ACTUAL=`./p3b.exe 50`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST H-4
echo -e "\n TEST H-4: ./p3b.exe 25 -----------------------------------------------"
EXPECTED="Battery: [*___]"
ACTUAL=`./p3b.exe 25`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST H-5
echo -e "\n TEST H-5: ./p3b.exe 24 -----------------------------------------------"
EXPECTED="Battery: [____]"
ACTUAL=`./p3b.exe 24`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- TICKET PRICE PROGRAM
# ---------------------------------------------------------------------- TEST I-1
echo -e "\n TEST I-1: ./p3c.exe 60 -----------------------------------------------"
EXPECTED="Ticket price: \$5.50"
ACTUAL=`./p3c.exe 60`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST I-2
echo -e "\n TEST I-2: ./p3c.exe 18 -----------------------------------------------"
EXPECTED="Ticket price: \$8.50"
ACTUAL=`./p3c 18`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST I-3
echo -e "\n TEST I-3: ./p3c.exe 12 -----------------------------------------------"
EXPECTED="Ticket price: \$9.75"
ACTUAL=`./p3c.exe 12`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST I-4
echo -e "\n TEST I-4: ./p3c.exe 11 -----------------------------------------------"
EXPECTED="Ticket price: \$6.25"
ACTUAL=`./p3c.exe 11`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi


# ---------------------------------------------------------------------- NUMBER RANGE PROGRAM 1
# ---------------------------------------------------------------------- TEST J-1
echo -e "\n TEST J-1: ./p4a.exe 6 -----------------------------------------------"
EXPECTED="6 is divisible by both 2 AND 3"
ACTUAL=`./p4a.exe 6`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST J-2
echo -e "\n TEST J-2: ./p4a.exe 4 -----------------------------------------------"
EXPECTED="4 is not divisible by both 2 AND 3"
ACTUAL=`./p4a.exe 4`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST J-3
echo -e "\n TEST J-3: ./p4a.exe 9 -----------------------------------------------"
EXPECTED="9 is not divisible by both 2 AND 3"
ACTUAL=`./p4a.exe 9`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST J-4
echo -e "\n TEST J-4: ./p4a.exe 5 -----------------------------------------------"
EXPECTED="5 is not divisible by both 2 AND 3"
ACTUAL=`./p4a.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- NUMBER RANGE PROGRAM 2
# ---------------------------------------------------------------------- TEST K-1
echo -e "\n TEST K-1: ./p4b.exe 6 -----------------------------------------------"
EXPECTED="6 is divisible by 2 OR by 3"
ACTUAL=`./p4b 6`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST K-2
echo -e "\n TEST K-2: ./p4b.exe 4 -----------------------------------------------"
EXPECTED="4 is divisible by 2 OR by 3"
ACTUAL=`./p4b.exe 4`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST K-3
echo -e "\n TEST K-3: ./p4b.exe 9 -----------------------------------------------"
EXPECTED="9 is divisible by 2 OR by 3"
ACTUAL=`./p4b.exe 9`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST K-4
echo -e "\n TEST K-4: ./p4b.exe 5 -----------------------------------------------"
EXPECTED="5 is not divisible by 2 OR by 3"
ACTUAL=`./p4b.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi

# ---------------------------------------------------------------------- NUMBER RANGE PROGRAM 3
# ---------------------------------------------------------------------- TEST L-1
echo -e "\n TEST L-1: ./p4c.exe 6 -----------------------------------------------"
EXPECTED="6 is divisible by 2 AND positive"
ACTUAL=`./p4c.exe 6`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST L-2
echo -e "\n TEST L-2: ./p4c.exe -6 -----------------------------------------------"
EXPECTED="-6 is not divisible by 2 OR is not positive"
ACTUAL=`./p4c.exe -6`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST L-3
echo -e "\n TEST L-3: ./p4c 5 -----------------------------------------------"
EXPECTED="5 is not divisible by 2 OR is not positive"
ACTUAL=`./p4c.exe 5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi
# ---------------------------------------------------------------------- TEST L-4
echo -e "\n TEST L-4: ./p4c.exe -5 -----------------------------------------------"
EXPECTED="-5 is not divisible by 2 OR is not positive"
ACTUAL=`./p4c.exe -5`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned [${ACTUAL}], was expecting [$EXPECTED].${NC}"
fi










