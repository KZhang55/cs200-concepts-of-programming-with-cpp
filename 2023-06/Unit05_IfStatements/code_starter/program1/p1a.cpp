#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create two float variables, for points and total.
 * Convert args[1] to the points variable.
 * Convert args[2] to the total varaible.
 * Create a new float to store the score.
 * Calculate the score with points / total * 100.
 * Use cout to display "Score: " and then the score, and a "%"
 * If the score is equal to 100, then also cout " (PERFECT)".
 * End of program
 * */

int main( int argCount, char* args[] )
{  
  if ( argCount < 3 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: points total" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
