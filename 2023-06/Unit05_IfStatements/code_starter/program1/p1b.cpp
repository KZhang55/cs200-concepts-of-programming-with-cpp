#include <iostream>
#include <iomanip>
using namespace std;

/* STUDENT NAME:
 * Create two float variables, for withdraw and balance.
 * Convert args[1] to the withdraw variable.
 * Convert args[2] to the balance varaible.
 * Create a new float to store the remaining balance.
 * Calculate the remaining balance with balance - withdraw.
 * Use cout to display "Balance: $" and then the balance
 *    Make sure you've used `cout << fixed << setprecision( 2 );`
 *    to set up formatting for USD style.
 * If the balance is less than 0, then also cout " (OVERDRAWN)".
 * End of program
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: withdraw balance" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
