#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create two float variables, for gas and capacity.
 * Convert args[1] to the gas variable.
 * Convert args[2] to the capacity varaible.
 * Create a new float to store the remaining gas.
 * Calculate the remaining gas with gas / capacity * 100.
 * Use cout to display "Remaining gas: " and then the remaining, and a "%"
 * If the remaining less than or equal to 10, then also cout " (LOW FUEL)".
 * End of program
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: gas capacity" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
