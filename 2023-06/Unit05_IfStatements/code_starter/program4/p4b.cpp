#include <iomanip>
#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create an integer variable to store a number. Convert args[1] to this variable.
 * If the number is divisible by 2 OR it's divisible by 3, then display number and " is divisible by 2 OR by 3"
 *    USE number % 2 == 0 to determine if number is divisible by 2
 *    USE number % 3 == 0 to determine if number is divisible by 3
 * Otherwise, display number and " is not divisible by 2 OR by 3"
 * */

int main(int argCount, char *args[]) {
  if ( argCount < 2 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: number" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
