#include <iomanip>
#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create an integer variable to battery charge. Convert args[1] to this variable.
 * Display "Battery: "
 * Use an if/else if/else statement to display the a "graphic" of the battery:
 *    90 and above: [****]
 *    75 and above: [***_]
 *    50 and above: [**__]
 *    25 and above: [*___]
 *    otherwise:    [____]
 * */

int main(int argCount, char *args[]) {
  if ( argCount < 2 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: chargepercent" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
