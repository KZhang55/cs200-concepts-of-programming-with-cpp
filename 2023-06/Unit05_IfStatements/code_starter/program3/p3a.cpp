#include <iomanip>
#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create a float variable to store the grade. Convert args[1] to this variable.
 * Display "Grade: "
 * Use an if/else if/else statement to display the letter grade:
 *    90 and above: A
 *    80 and above: B
 *    70 and above: C
 *    60 and above: D
 *    otherwise:    F
 * */

int main(int argCount, char *args[]) {
  if ( argCount < 2 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: scorepercent" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
