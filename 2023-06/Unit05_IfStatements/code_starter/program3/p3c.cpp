#include <iomanip>
#include <iostream>
using namespace std;

/* STUDENT NAME:
 * Create an integer variable to store a customer's age. Convert args[1] to this variable.
 * Display "Ticket price: $"
 * Use an if/else if/else statement to display the ticket price:
 *    60 and over: $5.50
 *    18 and over: $8.50
 *    12 and over: $9.75
 *    under 12:    $6.25
 * */

int main(int argCount, char *args[]) {
  if ( argCount < 2 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: age" << endl; return 1; }
  
  // Add code here
  
  return 0;
}
